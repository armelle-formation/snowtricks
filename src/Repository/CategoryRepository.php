<?php

namespace App\Repository;

use App\Entity\Category;
use App\Repository\BaseEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends BaseEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }
    
    /**
     * Get categories with online posts (active status + publication date) for a category ordered by name
     *
     * @return void
     */
    public function findCategoriesWithPostsPublishedOrderByName()
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = '
            SELECT COUNT(p.id) nb, c.title, c.slug
            FROM
                category c LEFT JOIN post p ON p.category_id = c.id
                LEFT JOIN user u ON p.author_id = u.id
            WHERE
                p.active = 1 AND
                p.date_publish <= Now() AND
                c.private = 0 AND
                u.active = 1
            GROUP BY p.category_id
            ORDER BY c.title ASC
        ';
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
   
    }
}
