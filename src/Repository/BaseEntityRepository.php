<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * Custom Repository base class containing specifics methodes to this application.
 *
 * To use in your repository class, change the extended class by BaseEntityRepository
 */
class BaseEntityRepository extends ServiceEntityRepository
{
    
    /**
     * Count items in this repository
     *
     * @return array
     */
    public function findAllByOrder($arguments)
    {
        $limit = array_key_exists('limit', $arguments) ? $arguments['limit'] : null;
        $offset = array_key_exists('offset', $arguments) ? $arguments['offset'] : null;
        return $this->findBy(
            array(),
            array($arguments['sortby'] => $arguments['order']),
            $limit,
            $offset
        );
    }
}
