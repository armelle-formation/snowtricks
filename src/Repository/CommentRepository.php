<?php

namespace App\Repository;

use App\Entity\Comment;
use App\Repository\BaseEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends BaseEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    /**
     * Returns all validate comments for a post
    * @return Comment[] Returns an array of Comment objects
    */
    public function findValidateCommentByPost($arguments)
    {
        $query = $this->createQueryBuilder('a')
            ->add('select', 'a')
            ->leftJoin('a.author', 'u')
            ->where('u.active = :useractive')
            ->setParameter('useractive', true)
            ->andWhere('a.status = :status')
            ->setParameter('status', $arguments['status'])
            ->andWhere('a.post = :post')
            ->setParameter('post', $arguments['post'])
            ->orderBy('a.' . $arguments['sortby'], $arguments['order']);
        
            if (array_key_exists('limit', $arguments) && array_key_exists('offset', $arguments)) {
                $query->setMaxResults($arguments['limit'])
                    ->setFirstResult($arguments['offset']);
            }

        return $query->getQuery()
                    ->getResult();
    }

}
