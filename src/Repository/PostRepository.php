<?php

namespace App\Repository;

use DateTime;
use App\Entity\Post;
use App\Repository\BaseEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends BaseEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
     * Get all posts online (active status + publication date) ordered from most recent to oldest
     *
     * @param integer $idCategory
     * @return void
     */
    public function findPublishedOrderRecently()
    {
        return $this->createQueryBuilder('a')
            ->add('select', 'a')
            ->leftJoin('a.category', 'c')
            ->where('c.private = :private')
            ->setParameter('private', false)
            ->leftJoin('a.author', 'u')
            ->andwhere('u.active = :useractive')
            ->setParameter('useractive', true)
            ->andWhere('a.datePublish <= :datepublish')
            ->setParameter('datepublish', new DateTime())
            ->andWhere('a.active = :active')
            ->setParameter('active', true)
            ->orderBy('a.datePublish', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Get all posts online (active status + publication date + author active) for a category ordered from most recent to oldest
     *
     * @param integer $idCategory
     * @return void
     */
    public function findPublishedOrderRecentlyByCategory(array $arguments)
    {
        $limit = array_key_exists('limit', $arguments) ? $arguments['limit'] : null;
        $offset = array_key_exists('offset', $arguments) ? $arguments['offset'] : null;

        return $this->createQueryBuilder('a')
            ->add('select', 'a')
            ->leftJoin('a.category', 'c')
            ->where('c.id LIKE :cat_id')
            ->setParameter('cat_id', $arguments['category']->getId())
            ->leftJoin('a.author', 'u')
            ->andwhere('u.active = :useractive')
            ->setParameter('useractive', true)
            ->andWhere('a.active = :active')
            ->setParameter('active', true)
             ->andWhere('c.private = :private')
            ->setParameter('private', false)
            ->andWhere('a.datePublish <= :datepublish')
            ->setParameter('datepublish', new DateTime())
            ->orderBy('a.' . $arguments['sortby'], $arguments['order'])
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    /**
     * Returns the last post of the footer category
     *
     * @param [type] $slug
     * @return void
     */
    public function findOneByCategorySlug($slug)
    {
        return $this->createQueryBuilder('a')
            ->add('select', 'a')
            ->leftJoin('a.category', 'c')
            ->where('c.slug LIKE :slug')
            ->setParameter('slug', $slug)
            ->orderBy('a.dateCreate', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
        ;
    }
    
    /**
     * Get all posts online (active status + publication date) ordered from most recent to oldest for an user (public account user page)
     *
     * @param integer $idCategory
     * @return void
     */
    public function findPublicPublishedPostOrderByUser($arguments)
    {
        $limit = array_key_exists('limit', $arguments) ? $arguments['limit'] : null;
        $offset = array_key_exists('offset', $arguments) ? $arguments['offset'] : null;
        $privateCat = array_key_exists('privateCat', $arguments) ? $arguments['privateCat'] : false;

        return $this->createQueryBuilder('a')
            ->add('select', 'a')
            ->leftJoin('a.category', 'c')
            ->where('c.private = :private')
            ->setParameter('private', $privateCat)
            ->leftJoin('a.author', 'u')
            ->andwhere('u.id = :user')
            ->setParameter('user', $arguments['user']->getId())
            ->andWhere('a.datePublish <= :datepublish')
            ->setParameter('datepublish', new DateTime())
            ->andWhere('a.active = :active')
            ->setParameter('active', true)
            ->orderBy('a.' . $arguments['sortby'], $arguments['order'])
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    /**
     * Get all posts of an user ordered from most recent to oldest (account user or active account)
     *
     * @param integer $idCategory
     * @return void
     */
    public function findPublicPostOrderByUser($arguments)
    {
        $limit = array_key_exists('limit', $arguments) ? $arguments['limit'] : null;
        $offset = array_key_exists('offset', $arguments) ? $arguments['offset'] : null;
        $privateCategories = array_key_exists('privateCat', $arguments) ? $arguments['privateCat'] : false;

        return $this->createQueryBuilder('p')
            ->add('select', 'p')
            ->leftJoin('p.category', 'c')
            ->where('c.private = :private')
            ->setParameter('private', $privateCategories)
            ->leftJoin('p.author', 'u')
            ->andwhere('u.id = :user')
            ->setParameter('user', $arguments['user']->getId())
            ->orderBy('p.' . $arguments['sortby'], $arguments['order'])
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }
}
