<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Post;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\Image;
use App\Entity\Video;
use App\Entity\Status;
use App\Entity\Comment;
use App\Entity\Category;
use App\Service\FileUploaderService;
use Faker\Provider\Youtube;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AppFixtures extends Fixture
{
    public function __construct(ParameterBagInterface $params, UserPasswordEncoderInterface $encoder, FileUploaderService $uploader)
    {
        $this->params = $params;
        $this->encoder = $encoder;
        $this->uploader = $uploader;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create($this->getLocale());
        $fakerVideo = \Faker\Factory::create();
        $fakerVideo->addProvider(new Youtube($fakerVideo));
        
        //Copy default images
        $this->uploader->copy($this->getFixturesImageDirectory() . 'tricks/default-trick.jpg', 'tricks', false);
        $this->uploader->copy($this->getFixturesImageDirectory() . 'avatars/default-avatar.png', 'avatar', false);

        //Images uploadées
        $imgTricks = [
            '5050_AdobeStock_106996650_Preview.jpeg',
            'AdobeStock_112818855_Preview.jpeg',
            'AdobeStock_116557709_Preview.jpeg',
            'AdobeStock_117091583_Preview.jpeg',
            'AdobeStock_185261060_Preview.jpeg',
            'backside_rodeo_1080_AdobeStock_286887053_Preview.jpeg',
            'cork_AdobeStock_124488050_Preview.jpeg',
            'japan_air_AdobeStock_319421540_Preview.jpeg',
            'mute_AdobeStock_188091891_Preview.jpeg',
            'shifty_AdobeStock_122252770_Preview.jpeg',
            'shifty_AdobeStock_304635000_Preview.jpeg',
            'tailGrab_AdobeStock_116557709_Preview.jpeg'
        ];

        $imgAvatarFemale = [
            'AdobeStock_202830484_a.jpg',
            'AdobeStock_202830484_d.jpg',
            'AdobeStock_202830484_f.jpg',
            'AdobeStock_202830484_w.jpg',
            'AdobeStock_202830484_k.jpg'
        ];
        $imgAvatarMale = [
            'AdobeStock_202830484_b.jpg',
            'AdobeStock_202830484_g.jpg',
            'AdobeStock_202830484_h.jpg',
            'AdobeStock_202830484_q.jpg',
            'AdobeStock_215365067_1000.jpg'
        ];
        
        
        //Roles
        $adminRole = new Role();
        $adminRole->setTitle('ROLE_ADMIN');
        $adminRole->setName('Administrateur');
        $manager->persist($adminRole);

        // Admin
        $adminUser = new User();
        $adminUser->setFirstName('Jeanne')
            ->setLastName('Doe')
            ->setPseudo('memelo')
            ->setEmail('admin')
            ->setHash($this->encoder->encodePassword($adminUser, 'password'))
            ->setAvatar('femme.png')
            ->setActive(1)
            ->setDateCreate($faker->dateTime($max = 'now', 'Europe/Paris'))
            ->addUserRole($adminRole);
        $manager->persist($adminUser);

        // Copy image admin
        $this->uploader->copy($this->getFixturesImageDirectory() . 'avatars/femme.png', 'avatar', false);

        //Users
        $users = [];
        for ($i = 1; $i <= 6; $i++) {

            $dateCreateUser = $faker->dateTimeBetween('2019-03-15 02:00:49', 'Europe/Paris');
            $dateUpdateUser = $faker->dateTimeBetween($dateCreateUser, 'Europe/Paris');
            $gender = in_array($i, [1,2,3]) ? 'Male' : 'Female';

            $user = new User();
            
            //Password
            $hash = $this->encoder->encodePassword($user, 'password');
            
            //Avatar : pick a random image, copy and delete froma array
            $imgFixture = ${"imgAvatar" . $gender}[mt_rand(0, count(${"imgAvatar" . $gender})-1)];
            $imgAvatar = $this->uploader->copy($this->getFixturesImageDirectory() . 'avatars/' . $imgFixture, 'avatar');
            if (($key = array_search($imgFixture, ${"imgAvatar" . $gender})) !== false) {
                array_splice(${"imgAvatar" . $gender}, $key, 1);
            }

            //Infos Users
            $user->setFirstName($faker->{'firstName' . $gender})
                ->setLastName($faker->lastName)
                ->setPseudo($faker->word())
                ->setEmail($faker->email)
                ->setAvatar($imgAvatar)
                ->setHash($hash)
                ->setActive($faker->randomElement([0,1]))
                ->setDateCreate($dateCreateUser)
                ->setDateUpdate($faker->dateTimeBetween($dateUpdateUser, 'Europe/Paris'));

            $manager->persist($user);
            $users[] = $user;
        }
        
        //Categories
        $listCategories = ['Rotations','Grabs', 'Flips', 'Slides'];
        $categories = [];
        for ($i = 0; $i <= count($listCategories) - 1; $i++) {
            $category = new Category();
            $category->setTitle($listCategories[$i]);
            $category->setPrivate(0);

            $manager->persist($category);
            $categories[] = $category;
        }

        //Status
        $aStatus = [];
        $counter = 1;
        $listStatus = ['A modérer','Validé', 'Masqué'];

        foreach ($listStatus as $libStatus) {
            $status = new Status();
            $status->setId($counter);
            $status->setName($libStatus);

            $manager->persist($status);
            $aStatus[] = $status;
            $counter++;
        }
     
        // Posts
        for ($i = 1; $i <= 12; $i++) {

            $title = $faker->sentence();
            $heading = $faker->paragraph(2);
            $dateCreatePost = $faker->dateTimeBetween('2019-03-15 02:00:49', 'Europe/Paris');
            $dateUpdatePost = $faker->dateTimeBetween($dateCreatePost, 'Europe/Paris');
            $content = '<p>' . join('<p></p>', $faker->paragraphs(5)) . '</p>';

            //Cover Image : pick a random image, copy and delete from array
            $imgCover = $imgTricks[mt_rand(0, count($imgTricks) - 1)];
            $coverImage = $this->uploader->copy($this->getFixturesImageDirectory() . 'tricks/' . $imgCover, 'tricks');
            if (($key = array_search($imgFixture, $imgTricks)) !== false) {
                array_splice($imgTricks, $key, 1);
            }

            // Details post
            $post = new Post();
            $post->setTitle($title)
                    ->setActive(1)
                    ->setDateCreate($dateCreatePost)
                    ->setDateUpdate($dateUpdatePost)
                    ->setDatePublish($faker->dateTimeBetween('-30 days', '+30 days', 'Europe/Paris'))
                    ->setContent($content)
                    ->setCoverImage($coverImage)
                    ->setAuthor($users[mt_rand(0, count($users) - 1)])
                    ->setCategory($categories[mt_rand(0, count($categories) - 1)]);
            
            //randomly add a heading
            if (mt_rand(0, 1)) {
                $post->setHeading($heading);
            }

            //Images
            for ($j = 1; $j <= mt_rand(1, 3); $j++) {
                $trickImg = $imgTricks[mt_rand(0, count($imgTricks) - 1)];
                $nameTrickImg = $this->uploader->copy($this->getFixturesImageDirectory() . 'tricks/' . $trickImg, 'tricks');
                $image = new Image();
                $image->setName($nameTrickImg)
                    ->setPost($post);

                $manager->persist($image);
            }
            
            //Videos
            for ($g = 1; $g <= mt_rand(1, 3); $g++) {
                $video = new Video();
                $video->setUrl($fakerVideo->youtubeEmbedUri())
                    ->setPost($post);

                $manager->persist($video);
            }

            
            // Commentaires
            if (mt_rand(0, 1)) {
                $comment = new Comment();

                $dateCreateComment = $faker->dateTimeBetween($dateCreatePost, 'Europe/Paris');
                $dateUpdateComment = $faker->dateTimeBetween($dateCreateComment, 'Europe/Paris');

                $comment->setContent($faker->paragraph())
                        ->setAuthor($users[mt_rand(0, count($users) - 1)])
                        ->setStatus($aStatus[mt_rand(0, count($aStatus) - 1)])
                        ->setPost($post)
                        ->setDateCreate($dateCreateComment)
                        ->setDateUpdate($dateUpdateComment);

                $manager->persist($comment);
            }

            $manager->persist($post);
        }

        //Private content
        //Who I am ?
        $whoCategory = new Category();
        $whoCategory->setTitle('Qui suis-je ?');
        $whoCategory->setPrivate(1);
        $manager->persist($whoCategory);

        $whoPost = new Post();
        $whoContent = "<p>Je m’appelle Lou, j’ai 31 ans et je vis à Toulouse, dans le Sud-Ouest de la France. Pas très loin de la montagne, pas trop loin de l’océan. Je dévale les pentes enneigées des Pyrénées l’hiver et j’aime surfer au Pays Basque ou dans les Landes l'été. Je passe le reste du temps sur mon skate (sauf les jours de pluie).</p>
        <p>N'hésitez à m’envoyer un petit mot...</p>";
        $whoPost->setTitle('Qui suis je ?')
                    ->setActive(1)
                    ->setDateCreate($dateCreatePost)
                    ->setDateUpdate($dateUpdatePost)
                    ->setDatePublish($faker->dateTime($timezone = 'Europe/Paris'))
                    ->setCoverImage('about.jpg')
                    ->setContent($whoContent)
                    ->setAuthor($adminUser)
                    ->setCategory($whoCategory);
        $manager->persist($whoPost);
        
        // Copy image Who I am ?
        $this->uploader->copy($this->getFixturesImageDirectory() . 'tricks/about.jpg', 'tricks');

        // About
        $aboutCategory = new Category();
        $aboutCategory->setTitle('A propos');
        $aboutCategory->setPrivate(1);
        $manager->persist($aboutCategory);

        $aboutPost = new Post();
        $aboutContent = "Ce site est un projet d'étude réalisé avec le framework Symfony 5 dans le cadre du parcours de formation d'OpenClassRooms : Développeur d'application PHP/Symfony.";
        $aboutPost->setTitle('A propos')
                    ->setActive(1)
                    ->setDateCreate($dateCreatePost)
                    ->setDateUpdate($dateUpdatePost)
                    ->setDatePublish($faker->dateTime($timezone = 'Europe/Paris'))
                    ->setContent($aboutContent)
                    ->setAuthor($adminUser)
                    ->setCategory($aboutCategory);

        $manager->persist($aboutPost);

        $manager->flush();
    }

    public function getPhotoUploadDir()
    {
        return $this->params->get('tricks_directory');
    }

    public function getPhotosAvatarsDir()
    {
        return $this->params->get('avatars_directory');
    }

    public function getLocale()
    {
        return $this->params->get('locale');
    }

    public function getFixturesImageDirectory()
    {
        return $this->params->get('fixtures_img_directory');
    }
    
}
