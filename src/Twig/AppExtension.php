<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('inarray', [$this, 'inArray']),
        ];
    }

    /**
     * @param string $variable
     * @param array $array
     *
     * @return bool
     */
    public function inArray(string $variable, array $array): bool
    {
        return in_array($variable, $array);
    }
}
