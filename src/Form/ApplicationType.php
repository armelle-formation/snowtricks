<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Core\Security;

class ApplicationType extends AbstractType
{
    
    protected $security;
    
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function getConfig($label, $attributes = [], $options = [])
    {
        return array_merge_recursive(
            [
                'label' => $label,
                'attr' => $attributes
            ],
            $options
        );
    }

}
