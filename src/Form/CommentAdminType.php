<?php

namespace App\Form;

use App\Entity\Status;
use App\Entity\Comment;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CommentAdminType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'content',
                TextareaType::class,
                $this->getConfig(
                    "Commentaire",
                    [
                        'placeholder' => "Contenu du commentaire",
                        'rows' => "4"
                    ],
                    ['required' => true]
                )
            )
            ->add(
                'status',
                EntityType::class,
                $this->getConfig(
                    'Status',
                    [],
                    [
                        'class' => Status::class,
                        'choice_label' => function ($status) {
                            return $status->getName();
                        }
                    ]
                )
            )
            ->add(
                'save',
                SubmitType::class,
                $this->getConfig(
                    'Enregistrer',
                    [
                        'class' => "btn btn-secondary"
                    ]
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
        ]);
    }
}
