<?php

namespace App\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class ReinitPasswordType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'password',
                RepeatedType::class,
                $this->getConfig(
                    "Mot de passe",
                    [
                        "class" => "password-field"
                    ],
                    [
                        'constraints' => [new Length(['min' => 8])],
                        'type' => PasswordType::class,
                        'invalid_message' => 'Les mots de passe doivent être identiques',
                        'required' => true,
                        'first_options'  => array(
                            'label' => 'Nouveau mot de passe password',
                            'attr' => array('placeholder' => 'Choisissez un nouveau mot de passe'),
                            'error_bubbling' => true
                        ),
                        'second_options' => array(
                            'label' => 'Confirmation mot de passe',
                            'attr' => array('placeholder' => 'Confirmez votre mot de passe'),
                            'error_bubbling' => true
                        ),
                        'error_bubbling' => true
                    ]
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
