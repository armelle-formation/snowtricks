<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class AccountType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $route = $options['data_route'];

        $builder
            ->add(
                'firstname',
                TextType::class,
                $this->getConfig(
                    "Prénom",
                    [ "placeholder" => "Votre prénom..." ]
                )
            )
            ->add(
                'lastname',
                TextType::class,
                $this->getConfig(
                    "Nom",
                    [ "placeholder" => "Votre Nom..." ]
                )
            )
            ->add(
                'pseudo',
                TextType::class,
                $this->getConfig(
                    "Pseudo",
                    [ "placeholder" => "Choisissez un pseudo..." ]
                )
            )
            ->add(
                'email',
                EmailType::class,
                $this->getConfig(
                    "Email",
                    [ "placeholder" => "Votre adresse email" ]
                )
            )
            ->add(
                'avatar',
                FileType::class,
                $this->getConfig(
                    "Image de profil",
                    [
                        'placeholder' => "Choisissez une image"
                    ],
                    [
                        "required" => false,
                        'mapped' => false,
                        'constraints' => [
                            new File([
                                'maxSize' => '4096k',
                                'mimeTypes' => [
                                    'image/gif',
                                    'image/jpeg',
                                    'image/png'
                                ],
                                'mimeTypesMessage' => "L'image n'est pas valide",
                            ])
                        ],
                    ]
                )
            )
            ->add(
                'avtChanged',
                HiddenType::class,
                [
                    'mapped' => false,
                    'data' => 0
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                $this->getConfig(
                    'Enregistrer',
                    [
                        'class' => "btn btn-secondary"
                    ]
                )
            )
        ;
    

        if ($route == 'admin_users_edit') {
            $builder->add(
                'active',
                CheckboxType::class,
                $this->getConfig(
                    false,
                    [
                        "data-toggle" => "switch",
                        "data-on-text" => "Actif",
                        "data-off-text" => "Inactif",
                        "data-size" => "small",
                    ],
                    [
                        'required' => false,
                    ]
                )
            );
        }

        if ($route == 'account_register') {
            $builder->add(
                'hash',
                PasswordType::class,
                $this->getConfig(
                    "Mot de passe",
                    [ "placeholder" => "Choisissez un mot de passe" ]
                )
            )
            ->add(
                'passwordConfirm',
                PasswordType::class,
                $this->getConfig(
                    "Confirmation de mot de passe",
                    [ "placeholder" => "Confirmer votre mot de passe"]
                )
            );
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'data_route'=>null
        ]);
    }
}
