<?php

namespace App\Form;

use App\Form\ApplicationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                $this->getConfig(
                    false,
                    [ "placeholder" => "Votre nom" ]
                )
            )
            ->add(
                'subject',
                TextType::class,
                $this->getConfig(
                    false,
                    [ "placeholder" => "Sujet du message" ]
                ))
            ->add(
                'email',
                EmailType::class,
                $this->getConfig(
                    false,
                    [ "placeholder" => "Votre email" ]
                ))
            ->add(
                'message',
                TextareaType::class,
                $this->getConfig(
                    false,
                    [ "placeholder" => "Votre message" ]
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
    }
}
