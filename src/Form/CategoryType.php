<?php

namespace App\Form;

use App\Entity\Category;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class CategoryType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                TextType::class,
                $this->getConfig(
                    "Titre",
                    [
                        'placeholder' => "Choississez un titre unique"
                    ]
                )
                )
            ->add(
                'private',
                CheckboxType::class,
                $this->getConfig(
                    false,
                    [
                        "data-toggle" => "switch",
                        "data-on-text" => "Privée",
                        "data-off-text" => "Publique",
                        "data-size" => "large",
                    ],
                    [
                        'required' => false
                    ]
                )
            )
            ->add(
                'save',
                SubmitType::class,
                $this->getConfig(
                    'Enregistrer',
                    [
                        'class' => "btn btn-secondary"
                    ]
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }
}
