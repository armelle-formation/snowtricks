<?php

namespace App\Form;

use App\Entity\Post;
use App\Entity\User;
use App\Form\ImageType;
use App\Form\VideoType;
use App\Entity\Category;
use App\Form\ApplicationType;
use Symfony\Component\Form\FormEvent;
use App\Repository\CategoryRepository;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class PostType extends ApplicationType
{
     
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $aPartsRoute = explode("_", $options['data_route']);
        $intersect = array_intersect($aPartsRoute, ['admin','posts']);

        $builder
            ->add(
                'active',
                CheckboxType::class,
                $this->getConfig(
                    false,
                    [
                        "data-toggle" => "switch",
                        "data-on-text" => "Publié",
                        "data-off-text" => "Brouillon",
                        "data-size" => "large",
                    ],
                    [
                        'required' => false
                    ]
                )
            )
            ->add(
                'title',
                TextType::class,
                $this->getConfig(
                    'Titre',
                    [
                        'placeholder' => "Nom du Trick"
                    ]
                )
            )
            ->add(
                'coverImage',
                FileType::class,
                $this->getConfig(
                    "Image de couverture",
                    [
                        'placeholder' => "Choisissez une image"
                    ],
                    [
                        "required" => false,
                        'mapped' => false,
                        'constraints' => [
                            new File([
                                'maxSize' => '4096k',
                                'mimeTypes' => [
                                    'image/gif',
                                    'image/jpeg',
                                    'image/png'
                                ],
                                'mimeTypesMessage' => "Le fichier n'est pas une image valide",
                            ])
                        ],
                    ]
                )
            )
            ->add(
                'category',
                EntityType::class,
                $this->getConfig(
                    'Catégorie',
                    [],
                    [
                        'class' => Category::class,
                        'placeholder' => 'Choisissez une catégorie',
                        'query_builder' => function (CategoryRepository $repo) {
                            return $repo->createQueryBuilder('c')
                                ->where('c.private = :private')
                                ->setParameter('private', false)
                                ->orderBy('c.title', 'ASC');
                        },
                        'choice_label' => function ($category) {
                            return $category->getTitle();
                        }
                    ]
                )
            )
            ->add(
                'datePublish',
                DateTimeType::class,
                $this->getConfig(
                    'Date de publication',
                    [],
                    [
                    'date_widget' => 'single_text',
                    'time_widget' => 'choice',
                    'date_format' => 'yyyy-MM-dd',
                    'minutes' => array(
                            '00' => '0',
                            '15' => '15',
                            '30' => '30',
                            '45' => '45',
                        ),
                    ]
                )
            )
            ->add(
                'heading',
                TextareaType::class,
                $this->getConfig(
                    'Chapo',
                    [],
                    [
                        'required' => false
                    ]
                )
            )
            ->add(
                'content',
                TextareaType::class,
                $this->getConfig(
                    "Contenu de l'article",
                    [
                        'placeholder' => "Description du trick"
                    ]
                )
            )
            ->add(
                'images',
                CollectionType::class,
                [
                    'required' => false,
                    'entry_type' => ImageType::class,
                    'label' => "Galerie d'images",
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false
                ],
                $this->getConfig("Galerie photo")
            )
            ->add(
                'videos',
                CollectionType::class,
                [
                    'entry_type' => VideoType::class,
                    'label' => "Galerie vidéos",
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'delete_empty' => true
                ] ,
                $this->getConfig("Galerie vidéo")
            )
            ->add(
                'coverChanged',
                HiddenType::class,
                [
                    'label' => false,
                    'mapped' => false,
                    'data' => 0
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                $this->getConfig(
                    'Enregistrer',
                    [
                        'class' => "btn btn-secondary"
                    ]
                )
            )
        ;

        // Differents options for Category field for admin
        if (count($intersect) == 2) {
            $builder->add(
                'category',
                EntityType::class,
                $this->getConfig(
                    'Catégorie',
                    [],
                    [
                        'class' => Category::class,
                        'choice_label' => 'title',
                        'placeholder' => 'Choisissez une catégorie',
                        'query_builder' => function (CategoryRepository $repo) {
                            return $repo->createQueryBuilder('c')
                                ->orderBy('c.title', 'ASC');
                        },
                        'choice_label' => function ($category) {
                            return $category->getTitle();
                        }
                    ]
                )
            );

            // PreSet Author
            $user = $this->security->getUser();

            $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($user) {
                if (null !== $event->getData()->getAuthor()) {
                    $user = $event->getData()->getAuthor();
                }
                $form = $event->getForm();
                $form->add(
                    'author',
                    EntityType::class,
                    $this->getConfig(
                        'Auteur',
                        [],
                        [
                            'placeholder' => "Sélectionnez un auteur",
                            'class' => User::class,
                            'choice_label' => function ($user) {
                                return $user->getFirstName() . " " . strtoupper($user->getLastName());
                            },
                            'data' => $user
                        ]
                    )
                );

            });

        } else {
            $builder->add(
                'category',
                EntityType::class,
                $this->getConfig(
                    'Catégorie',
                    [],
                    [
                        'class' => Category::class,
                        'placeholder' => 'Choisissez une catégorie',
                        'query_builder' => function (CategoryRepository $repo) {
                            return $repo->createQueryBuilder('c')
                                ->where('c.private = :private')
                                ->setParameter('private', false)
                                ->orderBy('c.title', 'ASC');
                        },
                        'choice_label' => function ($category) {
                            return $category->getTitle();
                        }
                    ]
                )
            );
        }
    }

    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
            'data_route'=>null
        ]);
    }
}
