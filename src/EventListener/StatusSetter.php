<?php
namespace App\EventListener;

use App\Entity\Status;
use App\Entity\Comment;
use Doctrine\ORM\Event\LifecycleEventArgs;

class StatusSetter
{
    /**
     * Overload prepersit LifeCycle for Comment
     * Set default status (to moderate)
     *
     * @param LifecycleEventArgs $args
     * @return void
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();
        
        if ($entity instanceof Comment && empty($entity->getStatus())) {
            $statusToModerate = $entityManager->getRepository(Status::class)->find(1);
            $entity->setStatus($statusToModerate);
        }
    }
}
