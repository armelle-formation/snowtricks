<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\Category;
use App\Entity\Traits\PostTrait;
use App\Repository\PostRepository;
use App\Service\PaginationService;
use App\Repository\CategoryRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{

    use PostTrait;

    /**
     * Display main page of the site
     * @Route("/", name="index")
     */
    public function index(PostRepository $repo)
    {
        $aPosts = $repo->findPublishedOrderRecently();
        
        return $this->render(
            'front/pages/home.html.twig',
            [
                'title' => 'Home',
                'backgroundColor' => 'background-transparent white-link',
                'logo' => 'snowtricks_logo_tspt_light_300',
                'posts' => $aPosts,
                'footerPost' => $this->getPostByCatSlug($this->getParameter('about_slug'))
            ]
        );
    }

    /**
     * Display posts by category
     * @Route("/category/{slug}/{page<\d+>?1}", name="category_show")
     */
    public function displayCategory(PostRepository $repoPost, CategoryRepository $repoCat, Category $category, $page, PaginationService $pagination)
    {
        $pagination->setEntityClass(Post::class)
                ->setPage($page)
                ->setLimit(5)
                ->setRoute('category_show')
                ->setMethod('findPublishedOrderRecentlyByCategory')
                ->setSortBy('dateCreate')
                ->setOrder('DESC')
                ->setTemplatePath('front/_partials/pagination.html.twig')
                ->setArguments(['category' => $category])
                ->setPaginationSlug($category->getSlug());

        // Aside
        $aCategories = $repoCat->findCategoriesWithPostsPublishedOrderByName();
        $aboutPost = $repoPost->findOneBy(['slug' => 'qui-suis-je']);
        
        return $this->render(
            'front/pages/posts/category.html.twig',
            [
                'pagination' => $pagination,
                'about' => $aboutPost,
                'categories' => $aCategories,
                'title' => $category->getTitle(),
                'footerPost' => $this->getPostByCatSlug($this->getParameter('about_slug'))
            ]
        );
    }
}
