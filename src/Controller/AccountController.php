<?php

namespace App\Controller;

use Exception;
use App\Entity\Post;
use App\Entity\User;
use App\Form\AccountType;
use App\Service\FileService;
use App\Service\EmailService;
use App\Service\TokenService;
use App\Entity\PasswordUpdate;
use App\Entity\Traits\PostTrait;
use App\Form\ForgotPasswordType;
use App\Form\PasswordUpdateType;
use App\Form\ReinitPasswordType;
use App\Repository\UserRepository;
use App\Service\PaginationService;
use App\Service\FileUploaderService;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AccountController extends AbstractController
{
    use PostTrait;

    /**
     * Display Login form and allows to connection
     * @Route("/login", name="account_login")
     * @return Response
     */
    public function login(AuthenticationUtils $utils)
    {
        $error = $utils->getLastAuthenticationError();
        $username = $utils->getLastUsername();

        return $this->render('/front/pages/account/login.html.twig', [
            'error' => $error,
            'username' => $username,
            'footerPost' => $this->getPostByCatSlug($this->getParameter('about_slug'))
        ]);
    }

    /**
     * Allows to disconnect
     * @Route("/logout", name="account_logout")
     * @return void
     */
    public function logout() {}

    /**
     * Allow to doisplay registration form
     * @Route("/register", name="account_register")
     * @return Response
     */
    public function register(
        Request $request,
        EntityManagerInterface $manager,
        EmailService $mailer,
        JWTEncoderInterface $jwtEncoder,
        TokenService $tokenService,
        ParameterBagInterface $params,
        UserPasswordEncoderInterface $encoder,
        FileUploaderService $fileUploader
    ) {
        $user = new User();
        $form = $this->createForm(AccountType::class, $user, [
            'data_route'=> $request->attributes->get('_route')
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setDateCreate(new \DateTime());
            
            //Images de profil
            $avatarImage = $form['avatar']->getData();
            if ($avatarImage === null) {
                $defaultImage = 'default-avatar.png';
                $user->setAvatar($defaultImage);
            } else {
                $avatarImageFilename = $fileUploader->upload($avatarImage, 'avatar');
                $user->setAvatar($avatarImageFilename);
            }

            //Password
            $hash = $encoder->encodePassword($user, $user->getHash());
            $user->setHash($hash);

            //Status
            $user->setActive(0);

            $manager->persist($user);
            $manager->flush();

            //Generate token : jwt valid for 30mn from the issued time (by default)
            $token = $tokenService->generateTokenForUser($jwtEncoder, $user);
                
            //Send confirmation email with account activation page link
            $subject = 'Confirmation de création de compte';
            $content = $this->renderView(
                'emails/registration.html.twig',
                [
                    'username' => $user->getFirstname() . " " . $user->getLastname(),
                    'id' => $user->getId(),
                    'token' => $token
                ],
                'text/html'
            );

            $return = $mailer->sendMessage($subject, $content, $user->getEmail(), $params->get('email-noreply'));

            // Personnalized return messages
            $result['success']['message'] = "Votre compte a bien été créé, un email de confirmation vous a été envoyé.";
            $result['warning']['message'] = "Votre compte a bien été créé, mais l'email de confirmation n'a pu être envoyé. Merci de contacter l'administrateur du site via le formulaire de contact.";
            $this->addFlash($return['label'], $result[$return['label']]['message']);
  
            return $this->redirectToRoute('account_register');
        }

        return $this->render('front/pages/account/registration.html.twig', [
            'form' => $form->createView(),
            'footerPost' => $this->getPostByCatSlug($this->getParameter('about_slug'))
        ]);
    }

    /**
     * Send confirm email for registration
     * @Route(path="/confirm-register/{token}", name="confirm_register")
     */
    public function confirmRegister(UserRepository $userRepo, Request $request, $token, JWTEncoderInterface $jwtEncoder, EntityManagerInterface $manager)
    {
        //Check if token exists
        if (!$token) {
            throw $this->createNotFoundException();
        }
        
        //Decode token
        $data = $jwtEncoder->decode($token);
        
        //Check if user exists
        $user = $userRepo->find($data['userid']);
        if (!$user instanceof User) {
            throw $this->createNotFoundException();
        }
        
        //Check email corresponding
        if ($data['email'] !== $user->getEmail()) {
            throw $this->createNotFoundException();
        }
        
        //Active and save account
        $user->setActive(1);
        
        //Update user account
        $manager->persist($user);
        $manager->flush();
        
        //Message for user
        $this->addFlash('success', "Votre compte a bien été activé. Vous pouvez désormais vous connecter");

        //Redirect on login page
        return $this->redirectToRoute('account_login');
    }

    /**
     * Display and process profile modification form
     * @Route("/account/profile", name="account_profile")
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function profile(Request $request, FileService $fileService, EntityManagerInterface $manager, FileUploaderService $fileUploader)
    {
        // User connect
        $user = $this->getUser();
        
        //paths
        $relativePath = $this->getParameter('avatars_img_rel_path');
        $absolutePath = $this->getParameter('avatars_directory');

        // Avatar
        $aAvatarInfos = [];

        $form = $this->createForm(AccountType::class, $user, [
            'data_route'=> $request->attributes->get('_route')
        ]);
        $form->remove('active');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            //Avatar image
            $avatarImage = $form['avatar']->getData();
            $oldAvatar = $user->getAvatar();

            if ($avatarImage !== null) {
                $avatarImageFilename = $fileUploader->upload($avatarImage, 'avatar');
                $user->setAvatar($avatarImageFilename);
            }
            if ($avatarImage === null && $form['avtChanged']->getData() == 1) {
                $user->setAvatar($avatarImage);
                if (!empty($oldAvatar)) {
                    $fileService->deleteFile($this->getParameter('avatars_directory') . $oldAvatar);
                }
            }

            $manager->persist($user);
            $manager->flush();
           
            $this->addFlash(
                'success',
                "Votre profil a bien été mis à jour"
            );
            
            return $this->redirectToRoute('account_profile');
        }

        if (!empty($user->getAvatar()) && $user->getAvatar() !== $this->getParameter('avatar_default')) {
            $aAvatarInfos['url'] = $relativePath . $user->getAvatar();
            $aAvatarInfos['size'] = filesize($absolutePath. $user->getAvatar());
        }

        return $this->render('front/pages/account/profile.html.twig', [
            'form' => $form->createView(),
            'imageAvatar' => json_encode($aAvatarInfos),
            'footerPost' => $this->getPostByCatSlug($this->getParameter('about_slug'))
        ]);
    }

    /**
     * Allow to update password
     * @Route("/account/password-update", name="account_password")
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function updatePassword(Request $request, UserPasswordEncoderInterface $encoder, EntityManagerInterface $manager)
    {
        $passwordUpdate = new PasswordUpdate();
        $user = $this->getUser();
        $form = $this->createForm(PasswordUpdateType::class, $passwordUpdate);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // check if oldPassword is the same of user password
            if (!$encoder->isPasswordValid($user, $passwordUpdate->getOldPassword())) {
                $form->get('oldPassword')->addError(new FormError("Votre ancien mot de passe est incorrect !"));
            } else {
                $newPassword = $passwordUpdate->getNewPassword();
                $hash = $encoder->encodePassword($user, $newPassword);

                $user->setHash($hash);

                $manager->persist($user);
                $manager->flush();

                $this->addFlash(
                    'success',
                    "Votre mot de passe a bien été modifié !"
                );
            }
        }


        return $this->render('front/pages/account/password.html.twig', [
            'form' => $form->createView(),
            'footerPost' => $this->getPostByCatSlug($this->getParameter('about_slug'))
        ]);
    }

    /**
     * Forgot password
     *
     * @Route("/forgot-password", name="account_forgot_password")
     *
     * @param Request $request
     * @param UserRepository $repo
     * @param ObjectManager $manager
     *
     * @return Response
     */
    public function forgotPassword(
        Request $request,
        UserRepository $userRepo,
        JWTEncoderInterface $jwtEncoder,
        TokenService $tokenService,
        EmailService $mailer,
        ParameterBagInterface $params
    ) {
        $form = $this->createForm(ForgotPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            //Check if user exists
            $user = $userRepo->findOneBy(['email' => $form['email']->getData()]);
            if (!$user instanceof User) {
                $this->addFlash(
                    'warning',
                    "Email inconnu"
                );
                return $this->redirectToRoute('account_forgot_password');
            }

            //Check if user is active
            if (!$user->getActive()) {
                $this->addFlash(
                    'warning',
                    "Votre compte est inactif"
                );
                return $this->redirectToRoute('account_forgot_password');
            }

            //Generate token : jwt valid for 2h from the issued time
            $token = $tokenService->generateTokenForUser($jwtEncoder, $user, 120);
                
            //Send email with reinit password form link
            $subject = 'Réinitialisation de mot de passe';
            $content = $this->renderView(
                'emails/forgot-password.html.twig',
                [
                    'username' => $user->getFirstname() . " " . $user->getLastname(),
                    'id' => $user->getId(),
                    'token' => $token
                ],
                'text/html'
            );
            $return = $mailer->sendMessage($subject, $content, $user->getEmail(), $params->get('email-noreply'));
            
            // Personnalized return messages
            $result['success']['message'] = "Votre demande de réinitialisation de mot de passe a bien été traitée, un email vous a été envoyé.";
            $result['warning']['message'] = "Votre demande de réinitialisation de mot de passe  a échouée. Merci de contacter l'administrateur du site via le formulaire de contact.";
            $this->addFlash($return['label'], $result[$return['label']]['message']);
        }
            
        return $this->render('front/pages/account/forgot-password.html.twig', [
            'form' => $form->createView(),
            'footerPost' => $this->getPostByCatSlug($this->getParameter('about_slug'))
        ]);
    }

    /**
     * Reset password (forgot)
     *
     * @Route("/reinit-password/{token}", name="account_password_reinit")
     *
     * @param Request $request
     * @param UserRepository $repo
     * @param ObjectManager $manager
     * @param UserPasswordEncoderInterface $encoder
     *
     * @return Response
     */
    public function reinitPassword(
        Request $request,
        $token,
        UserRepository $userRepo,
        EntityManagerInterface $manager,
        JWTEncoderInterface $jwtEncoder,
        UserPasswordEncoderInterface $encoder
    ) {
        try {
            //Check if token exists
            if (!$token) {
                throw $this->createNotFoundException();
            }
            
            //Decode token
            $data = $jwtEncoder->decode($token);
            
            //Check if user exists
            $user = $userRepo->find($data['userid']);
            if (!$user instanceof User) {
                throw new AccessDeniedHttpException();
            }
            
            //Check if user is active
            if (!$user->getActive()) {
                throw new AccessDeniedHttpException();
            }

            //Check email corresponding
            if ($data['email'] !== $user->getEmail()) {
                throw new AccessDeniedHttpException();
            }
            
            //DisplayForm
            $form = $this->createForm(ReinitPasswordType::class);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                
                //Password
                $hash = $encoder->encodePassword($user, $form['password']->getData());
                $user->setHash($hash);

                //Update user account
                $manager->persist($user);
                $manager->flush();

                //Message flash
                $this->addFlash(
                    'success',
                    "Votre mot de passe a bien été modifié !"
                );

                //Redirect on login page
                return $this->redirectToRoute('account_login');
            }

            return $this->render('front/pages/account/reinit-password.html.twig', [
                'form' => $form->createView(),
                'footerPost' => $this->getPostByCatSlug($this->getParameter('about_slug'))
            ]);
            
        } catch (JWTDecodeFailureException $e) {
            $this->addFlash(
                'warning',
                "Le lien de réinitialisation a expiré. Merci de refaire une demande"
            );
            return $this->redirectToRoute('account_forgot_password');
 
        } catch (Exception $e) {
            throw new AccessDeniedHttpException();
        }
    }


    /**
     * Display profil of logged user
     *
     * @Route("/account/{page<\d+>?1}", name="account_index")
     * @IsGranted("ROLE_USER")
     *
     * @return Response
     */
    public function renderAccount(PaginationService $pagination, $page)
    {
        $pagination->setEntityClass(Post::class)
                ->setPage($page)
                ->setLimit(6)
                ->setMethod('findPublicPostOrderByUser')
                ->setSortBy('dateCreate')
                ->setOrder('DESC')
                ->setTemplatePath('front/_partials/pagination.html.twig')
                ->setArguments(['user' => $this->getUser(), 'privateCat' => false ])
                ->setRoute('account_index');

        return $this->render('front/pages/user/index.html.twig', [
            'user' => $this->getUser(),
            'pagination' => $pagination,
            'footerPost' => $this->getPostByCatSlug($this->getParameter('about_slug'))
        ]);
    }
}
