<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\Image;
use App\Form\PostType;
use App\Service\FileService;
use App\Service\VideoService;
use App\Service\PurifierService;
use App\Repository\PostRepository;
use App\Service\PaginationService;
use App\Repository\ImageRepository;
use App\Service\FileUploaderService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormInterface;

class AdminPostController extends AbstractController
{
    
    /**
     *  Admin : display list of posts
     * @Route("/admin/{page<\d+>?1}", name="admin_index")
     * @Route("/admin/posts/{page<\d+>?1}", name="admin_posts_index")
     * @param PostRepository $repo
     * @param int $page
     * @param PaginationService $pagination
     * @return Response
     */
    public function index(PostRepository $repo, $page, PaginationService $pagination)
    {
        $pagination->setEntityClass(Post::class)
                    ->setPage($page)
                    ->setSortBy('dateCreate')
                    ->setOrder('DESC');

        return $this->render('admin/pages/post/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * Admin : add a post
     * @Route("/admin/post/add", name="admin_posts_add")
     * @param Post $post
     * @return Response
     */
    public function add(Request $request, EntityManagerInterface $manager, FileUploaderService $fileUploader)
    {
        $post = new Post();
        $post->setDatePublish(new \DateTime());

        $form = $this->createForm(PostType::class, $post, [
            'data_route'=> $request->attributes->get('_route')
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                //Cover image
                $coverImage = $form['coverImage']->getData();

                if ($coverImage !== null) {
                    $coverImageFilename = $fileUploader->upload($coverImage, 'tricks');
                    $post->setCoverImage($coverImageFilename);
                } else {
                    $post->setCoverImage($coverImage); //TODO : Unlink avatar image
                }

                //Gallery images
                $aImages = $request->files->get('images');
                $i = 1;
                foreach ($aImages as $image) {
                    
                    $GalleryFilename = $fileUploader->upload($image, 'tricks');
                    $image = new Image();
                    $image->setName($GalleryFilename);
                    $post->addImage($image);
                    $i++;
                }
                
                // SaveCommonDatas
                $this->saveCommonDataPost($post, $form, $manager);
                       
                return $this->redirectToRoute('admin_posts_edit', [
                    'id' => $post->getId()
                ]);
                
            } else {
                $this->addFlash(
                    'warning',
                    "Une erreur s'est produite, l'article n'a pas été enregistré. <br >Merci de vérifiez les messages d'erreurs du formulaire."
                );
            }
        }
        return $this->render('admin/pages/post/new.html.twig', [
            'form' => $form->createView()
        ]);
    }


     /**
     * Admin : edit a post
     * @Route("/admin/post/{id}/edit", name="admin_posts_edit")
     * @param Post $post
     * @return Response
     */
    public function edit(
        Post $post,
        Request $request,
        FileService $fileService,
        ImageRepository $imageRepo,
        EntityManagerInterface $manager,
        FileUploaderService $fileUploader,
        ParameterBagInterface $params
    ) {
        //paths
        $relativePath = $this->getParameter('tricks_img_rel_path');
        $absolutePath = $this->getParameter('tricks_directory');

        // Cover Image
        $aImageCover = [];
                
        // Images gallery
        $aImagesUrl = [];
        $aImagesInfos = [];
        $aImagesIds = [];

        // Get images of post
        $aImages = $imageRepo->findBy(array('post' => $post->getId()));
        foreach ($aImages as $image) {
            array_push($aImagesUrl, $relativePath.$image->getName());
            array_push(
                $aImagesInfos,
                [
                    'size' => filesize($absolutePath. $image->getName()),
                    'id' => $image->getId()
                ]
            );
            array_push($aImagesIds, $image->getId());
        }

        $form = $this->createForm(PostType::class, $post, [
            'data_route'=> $request->attributes->get('_route')
        ]);
        $form->add('old-images', HiddenType::class, ['mapped' => false, 'data' => implode(",", $aImagesIds)]);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
            
                //Cover image
                $coverImage = $form['coverImage']->getData();
                $oldCoverImage = $post->getCoverImage();

                if ($coverImage !== null) {
                    $trickImageFilename = $fileUploader->upload($coverImage, 'tricks');
                    $post->setCoverImage($trickImageFilename);
                }
                if ($coverImage == null && $form['coverChanged']->getData() == 1) {
                    $post->setCoverImage($coverImage);
                    if (!empty($oldCoverImage)) {
                        $fileService->deleteFile($this->getParameter('tricks_directory') . $oldCoverImage);
                    }
                }
        
                //Gallery images
                $aNewImages = $request->files->get('images');
                $aOldsImages = !empty($form['old-images']->getData()) ? explode(",", $form['old-images']->getData()) : array();
            
                if (count($aNewImages)) {
                    $i = 1;
                    foreach ($aNewImages as $image) {

                        $GalleryFilename = $fileUploader->upload($image, 'tricks');
                        $image = new Image();
                        $image->setName($GalleryFilename);
                        $post->addImage($image);
                        $i++;
                    }
                }
                
                if (count($aOldsImages)) {

                    //Keep old images not deleted
                    $j = 1;
                    foreach ($aOldsImages as $idImage) {
                        $image = $imageRepo->find($idImage);
                        $post->addImage($image);
                        $j++;
                    }

                    // Delete old deleted images
                    $aIdsImgToDelete = array_diff($aImagesIds, $aOldsImages);
                    foreach ($aIdsImgToDelete as $idImage) {
                        $imageToDel = $imageRepo->find($idImage);
                        $fileService->deleteFile($this->getParameter('tricks_directory') . $imageToDel->getName());
                    }
                }

                // Update
                $post->setDateUpdate(new \DateTime());
                
                // SaveCommonDatas
                $this->saveCommonDataPost($post, $form, $manager);

                return $this->redirectToRoute('admin_posts_edit', [
                    'id' => $post->getId()
                ]);
                
            } else {
                $this->addFlash(
                    'success',
                    "Une erreur s'est produite, l'article <strong>{$post->getTitle()}</strong> n'a pas pu être modifié. <br >Merci de vérifiez les messages d'erreur du formulaire."
                );
            }
        }
        
        if (!empty($post->getCoverImage()) && $post->getCoverImage() !== $params->get('trick_cover_default')) {
            if ($fileService->exists($absolutePath . $post->getCoverImage())) {
                $aImageCover['url'] = $relativePath . $post->getCoverImage();
                $aImageCover['size'] = filesize($absolutePath . $post->getCoverImage());
            }
        }

        return $this->render('admin/pages/post/edit.html.twig', [
            'form' => $form->createView(),
            'urls' => json_encode($aImagesUrl),
            'imagesInfos' => json_encode($aImagesInfos),
            'imageCover' => json_encode($aImageCover),
            'post' => $post
        ]);
    }

    
    /**
     * Admin : delete post (after confirm)
     * @Route("/admin/posts/delete", name="admin_posts_delete")
     * @param Post $post
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(PostRepository $repo, FileService $fileService, Request $request, EntityManagerInterface $manager)
    {
           
        $form = $this->createFormBuilder(array())
            ->add('id', HiddenType::class)
            ->getForm();
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {
                $post = $repo->find($form['id']->getData());
                $aPostImages = $post->getImages();
                $oldCoverImage = $post->getCoverImage();

                //Delete CoverImage
                if (!empty($oldCoverImage)) {
                    $fileService->deleteFile($this->getParameter('tricks_directory') . $oldCoverImage);
                }

                //Delete Posts Image
                if (count($aPostImages)) {
                    foreach ($aPostImages as $file) {
                        $fileService->deleteFile($this->getParameter('tricks_directory') . $file->getName());
                    }
                }

                $manager->remove($post);
                $manager->flush();

                $this->addFlash(
                    'success',
                    "L'article <strong>{$post->getTitle()}</strong> a bien été supprimé !"
                );
            }
        } else {
            $this->addFlash(
                'warning',
                "Vous n'avez pas l'autorisation de supprimer cet article !"
            );
        }
        
        return $this->redirectToRoute('admin_posts_index');
    }

    /**
     * Admin : Display popin with confirmation message for delete a post
     * @Route("/admin/posts/confirm/delete/{id}", name="admin_posts_confirm_delete")
     * @return Response
     */
    public function confirmDelete(int $id)
    {
        $form = $this->createFormBuilder(array())
            ->add('id', HiddenType::class, [
                'data' => $id
            ])
            ->getForm();

        return $this->render('admin/blocs/confirm-modal-form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Confirmation de suppression',
            'text' => 'Etes vous sur de vouloir supprimer cet article ?',
            'action' => $this->generateUrl('admin_posts_delete'),
            'idForm' => 'confirm-delete-' . $id
        ]);

    }

    /**
     * Reduces code duplication when saving a post
     *
     * @param Post $post
     * @param FormInterface $form
     * @param EntityManagerInterface $manager
     * @return void
     */
    private function saveCommonDataPost(Post $post, FormInterface $form, EntityManagerInterface $manager)
    {
        //Service
        $videoService = new VideoService();
        $purifier = new PurifierService();

        //Videos
        $result = $videoService->addVideosToPost($post, $form['videos']->getData());
        if (!empty($result)) {
            $this->addFlash($result['label'], $result['message']);
        }
         
        //ContentSanitize
        $content = $purifier->purify($form['content']->getData());
        $post->setContent($content);

        $manager->persist($post);
        $manager->flush();
        
        $this->addFlash(
            'success',
            "L'article <strong>{$post->getTitle()}</strong> a bien été enregistrée !"
        );
    }

}
