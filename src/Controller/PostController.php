<?php

namespace App\Controller;

use DateTime;
use App\Entity\Post;
use App\Entity\Image;
use App\Form\PostType;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Service\FileService;
use App\Service\VideoService;
use App\Entity\Traits\PostTrait;
use App\Service\PurifierService;
use App\Repository\PostRepository;
use App\Service\PaginationService;
use App\Repository\ImageRepository;
use App\Repository\StatusRepository;
use App\Service\FileUploaderService;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class PostController extends AbstractController
{
    use PostTrait;

    /**
     * Create a new Post
     * @Route("tricks/new", name="trick_create")
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function create(
        Request $request,
        PurifierService $purifier,
        EntityManagerInterface $manager,
        VideoService $videoService,
        FileUploaderService $fileUploader
    ) {

        $post = new Post();
        $post->setDatePublish(new \DateTime());

        $form = $this->createForm(PostType::class, $post);
        $form->remove('author');
        $form->handleRequest($request);
 
        if ($form->isSubmitted() && $form->isValid()) {
            
            //Cover image
            $coverImage = $form['coverImage']->getData();
            if ($coverImage === null) {
                $defaultImage = 'default-trick.jpg';
                $post->setCoverImage($defaultImage);
            } else {
                $coverImageFilename = $fileUploader->upload($coverImage, 'tricks');
                $post->setCoverImage($coverImageFilename);
            }

            //Gallery images
           $aImages = $request->files->get('images');
            $i = 1;
            foreach ($aImages as $image) {
                
                $GalleryFilename = $fileUploader->upload($image, 'tricks');
                $image = new Image();
                $image->setName($GalleryFilename);
                $post->addImage($image);
                $i++;
            }
            
            //Videos
            $result = $videoService->addVideosToPost($post, $form['videos']->getData());
            if (!empty($result)) {
                $this->addFlash($result['label'], $result['message']);
            }

            // ContentSanitize
            $content = $purifier->purify($form['content']->getData());
            $post->setContent($content);

            // Author
            $post->setAuthor($this->getUser());

            $manager->persist($post);
            $manager->flush();

            $this->addFlash(
                "success",
                "Le trick <strong>{$post->getTitle()}</strong> a bien été enregistré !"
            );

            // Redirection to post if datePublish not greater than today
            if ($post->getDatePublish() <= new DateTime('now') && $post->getActive()) {
                return $this->redirectToRoute('trick_show', [
                    'slug' => $post->getSlug()
                ]);
            } else {
                return $this->redirectToRoute('trick_edit', [
                    'slug' => $post->getSlug()
                ]);
            }
        }
        return $this->render('front/pages/posts/new.html.twig', [
            'form' => $form->createView(),
            'footerPost' => $this->getPostByCatSlug($this->getParameter('about_slug'))
        ]);
    }

    /**
     * Edit post
     * @Route("tricks/{slug}/edit", name="trick_edit")
     * @Security("is_granted('ROLE_USER') and user === post.getAuthor()", message="Cette annonce ne vous appartient pas, vous ne pouvez pas la modifier")
     * @return void
     */
    public function edit(
        Post $post,
        Request $request,
        PurifierService $purifier,
        FileService $fileService,
        ImageRepository $imageRepo,
        VideoService $videoService,
        EntityManagerInterface $manager,
        FileUploaderService $fileUploader
    ) {
        //Paths
        $relativePath = $this->getParameter('tricks_img_rel_path');
        $absolutePath = $this->getParameter('tricks_directory');

        // CoverImage
        $aImageCover = [];

        // Gallery images
        $aImages = $imageRepo->findBy(array('post' => $post->getId()));
        $aImagesUrl = [];
        $aImagesInfos = [];
        $aImagesIds = [];

        foreach ($aImages as $image) {
            array_push($aImagesUrl, $relativePath.$image->getName());
            array_push(
                $aImagesInfos,
                [
                    'size' => filesize($absolutePath. $image->getName()),
                    'id' => $image->getId()
                ]
            );
            array_push($aImagesIds, $image->getId());
        }

        // Form
        $form = $this->createForm(PostType::class, $post);
        $form->add('old-images', HiddenType::class, ['mapped' => false, 'data' => implode(",", $aImagesIds)]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //Cover image
            $coverImage = $form['coverImage']->getData();
            $oldCoverImage = $post->getCoverImage();
            
            if ($coverImage !== null) {
                $trickImageFilename = $fileUploader->upload($coverImage, 'tricks');
                $post->setCoverImage($trickImageFilename);
            }
            if ($coverImage == null && $form['coverChanged']->getData() == 1) {
                $post->setCoverImage($coverImage);
                if (!empty($oldCoverImage)) {
                    $fileService->deleteFile($this->getParameter('tricks_directory') . $oldCoverImage);
                }
            }

            //Gallery images
            $aNewImages = $request->files->get('images');
            $aOldsImages = !empty($form['old-images']->getData()) ? explode(",", $form['old-images']->getData()) : array();

            if (count($aNewImages)) {
                $i = 1;
                foreach ($aNewImages as $image) {
                    $GalleryFilename = $fileUploader->upload($image, 'tricks');
                    $image = new Image();
                    $image->setName($GalleryFilename);
                    $post->addImage($image);
                    $i++;
                }
            }

            if (count($aOldsImages)) {

                //Keep old images not deleted
                $j = 1;
                foreach ($aOldsImages as $idImage) {
                    $image = $imageRepo->find($idImage);
                    $post->addImage($image);
                    $j++;
                }

                // Delete old deleted images
                $aIdsImgToDelete = array_diff($aImagesIds, $aOldsImages);
                foreach ($aIdsImgToDelete as $idImage) {
                    $imageToDel = $imageRepo->find($idImage);
                    $fileService->deleteFile($this->getParameter('tricks_directory') . $imageToDel->getName());
                }
            }

            //Videos
            $result = $videoService->addVideosToPost($post, $form['videos']->getData());
            if (!empty($result)) {
                $this->addFlash($result['label'], $result['message']);
            }
            
            //ContentSanitize
            $content = $purifier->purify($form['content']->getData());
            $post->setContent($content);

            $manager->persist($post);
            $manager->flush();

            $this->addFlash(
                "success",
                "Le trick <strong>{$post->getTitle()}</strong> a bien été modifié !"
            );

            // Redirection to post if datePublish not greater than today
            if ($post->getDatePublish() <= new DateTime('now') && $post->getActive()) {
                return $this->redirectToRoute('trick_show', [
                    'slug' => $post->getSlug()
                ]);
            } else {
                return $this->redirectToRoute('trick_edit', [
                    'slug' => $post->getSlug()
                ]);
            }
        }

        if (!empty($post->getCoverImage() && $post->getCoverImage() !== $this->getParameter('trick_cover_default'))) {
            $aImageCover['url'] = $relativePath . $post->getCoverImage();
            $aImageCover['size'] = filesize($absolutePath. $post->getCoverImage());
        }

        return $this->render('front/pages/posts/edit.html.twig', [
            'form' => $form->createView(),
            'urls' => json_encode($aImagesUrl),
            'imagesInfos' => json_encode($aImagesInfos),
            'imageCover' => json_encode($aImageCover),
            'post' => $post,
            'footerPost' => $this->getPostByCatSlug($this->getParameter('about_slug'))
        ]);
    }

    /**
     * Show an post
     * @Route("/tricks/{slug}/{page<\d+>?1}", name="trick_show")
     * @param PaginationService $pagination
     * @param Post $oPost
     * @return Response
     */
    public function show(
        Post $oPost,
        $page,
        Request $request,
        PostRepository $repoPost,
        PaginationService $pagination,
        CategoryRepository $repoCat,
        StatusRepository $repoStatus,
        EntityManagerInterface $manager,
        ParameterBagInterface $params
    ) {
        // Redirection to 404 if datePublish is greater than today and user isn't author or author is inactive
        if (($oPost->getDatePublish() > new DateTime('now') && $this->getUser()->getId() != $oPost->getAuthor()->getId()) || !$oPost->getAuthor()->getActive()) {
            throw $this->createNotFoundException("Cet article n'existe pas");
        }

        // Aside
        $aCategories = $repoCat->findCategoriesWithPostsPublishedOrderByName();
        $aboutPost = $repoPost->findOneBy(['slug' => 'qui-suis-je']);

        //Comments of trick
        $activeStatus = $repoStatus->find(2);
        $pagination->setEntityClass(Comment::class)
                    ->setPage($page)
                    ->setSortBy('dateCreate')
                    ->setOrder('DESC')
                    ->setLimit(5)
                    ->setRoute('trick_show')
                    ->setTemplatePath('front/_partials/pagination.html.twig')
                    ->setMethod('findValidateCommentByPost')
                    ->setArguments(['post'=> $oPost, 'status'=> $activeStatus])
                    ->setPaginationSlug($oPost->getSlug());

                    
        $comments = $pagination->getData();
        $nbComments = $pagination->getTotal();
        
        // Comment form
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
           
            $comment->setPost($oPost)
                    ->setAuthor($this->getUser());
                    
            $manager->persist($comment);
            $manager->flush();

            $this->addFlash(
                'success',
                "Votre commentaire a bien été pris en compte !"
            );

            $form = $this->createForm(CommentType::class);
        }

        // Status instance
        return $this->render('front/pages/posts/post.html.twig', [
            'post' => $oPost,
            'comments' => $comments,
            'nbComments' => $nbComments,
            'about' => $aboutPost,
            'categories' => $aCategories,
            'title' => $oPost->getTitle(),
            'form' => $form->createView(),
            'pagination' => $pagination,
            'footerPost' => $this->getPostByCatSlug($params->get('about_slug'))
        ]);
    }

    /**
     * Display popin with confirmation message for delete a post
     * @Route("/trick/{id}/delete/confirm", name="trick_confirm_delete")
     * @Security(
     *      "is_granted('ROLE_ADMIN') or (is_granted('ROLE_USER') and user == post.getAuthor())",
     *      message="Ce post ne vous appartient pas, vous n'avez pas le droit de le supprimer"
     * )
     * @param Post $oPost
     * @return Response
     */
    public function renderFormPostDelete(Post $post, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createFormBuilder(array())
            ->add('id', HiddenType::class, [
                'data' => $post->getId()
            ])
            ->getForm();
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->remove($post);
            $manager->flush();
    
            $this->addFlash(
                'success',
                "L'article a bien été supprimé !"
            );
            
            return $this->redirectToRoute('account_index');
        }

        return $this->render('front/blocs/confirm-modal.html.twig', [
            'form' => $form->createView(),
            'post' => $post
        ]);
    }
}
