<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AccountType;
use App\Service\FileService;
use App\Repository\UserRepository;
use App\Service\PaginationService;
use App\Service\FileUploaderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminUserController extends AbstractController
{
    /**
     * Admin : display list of users
     * @Route("/admin/user/{page<\d+>?1}", name="admin_users_index")
     * @param int $page
     * @param PaginationService $pagination
     * @return Response
     */
    public function index($page, PaginationService $pagination)
    {
        $pagination->setEntityClass(User::class)
                ->setSortBy('dateCreate')
                ->setOrder('DESC')
                ->setPage($page);

        return $this->render('admin/pages/user/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * Admin : edit an user
     * @Route("/admin/user/{id}/edit", name="admin_users_edit")
     * @param Post $post
     * @return Response
     */

    public function edit(User $user, Request $request, FileService $fileService, EntityManagerInterface $manager, FileUploaderService $fileUploader)
    {
        //paths
        $relativePath = $this->getParameter('avatars_img_rel_path');
        $absolutePath = $this->getParameter('avatars_directory');

        // Avatar
        $aAvatarInfos = [];
        $oldAvatar = $user->getAvatar();

        $form = $this->createForm(AccountType::class, $user, [
            'data_route'=> $request->attributes->get('_route')
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //Avatar image
            $avatarImage = $form['avatar']->getData();

            if ($avatarImage !== null) {
                $avatarImageFilename = $fileUploader->upload($avatarImage, 'avatar');
                $user->setAvatar($avatarImageFilename);
            }
            if ($avatarImage === null && $form['avtChanged']->getData() == 1) {
                $user->setAvatar($avatarImage);
                if (!empty($oldAvatar)) {
                    $fileService->deleteFile($this->getParameter('avatars_directory') . $oldAvatar);
                }
            }
            
            //Statut
            if ($user === $this->getUser() && !$form['avatar']->getData()) {
                $this->addFlash(
                    'warning',
                    "Vous ne pouvez pas vous désactiver vous même !"
                );
                return $this->redirectToRoute('admin_users_edit', ['id' => $user->getId()]);
            }

            //Update
            $user->setDateUpdate(new \DateTime());

            $manager->persist($user);
            $manager->flush();

            $this->addFlash(
                'success',
                "L'utilisateur <strong>{$user->getFullName()}</strong> a bien été modifié !"
            );
            
            return $this->redirectToRoute('admin_users_edit', ['id' => $user->getId()]);
        }

        if (!empty($user->getAvatar()) && $user->getAvatar() !==  $this->getParameter('avatar_default')) {
            $aAvatarInfos['url'] = $relativePath . $user->getAvatar();
            $aAvatarInfos['size'] = filesize($absolutePath. $user->getAvatar());
        }

        return $this->render('admin/pages/user/edit.html.twig', [
            'form' => $form->createView(),
            'imageAvatar' => json_encode($aAvatarInfos),
            'user' => $user
        ]);

    }

    /**
     * Admin : Display popin with confirmation message for delete an user
     * @Route("/admin/users/confirm/delete/{id}", name="admin_user_confirm_delete")
     * @IsGranted("ROLE_ADMIN")
     * @param Post $user
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function confirmDelete(User $user)
    {
        $options = [
            'title' => 'Confirmation de suppression',
            'text' => 'Vous ne pouvez pas supprimer cette utilisateur '
        ];
        $view = 'admin/blocs/modal-info.html.twig';

        if ($user === $this->getUser()) {
            $options['text'] = "Vous ne pouvez pas supprimer l'utilisateur courant !";
        } elseif (count($user->getPosts()) > 0) {
            $options['text'] .= " il possède des articles ! <br />Vous devez les réatribuer à un autre contributeur.";
        }  else {
            $view = 'admin/blocs/confirm-modal-form.html.twig';
            $options['text'] = 'Etes vous sur de vouloir supprimer cet utilisateur ?';
            $options['action'] = $this->generateUrl('admin_users_delete');
            $options['idForm'] = 'confirm-delete-' . $user->getId();

            $form = $this->createFormBuilder(array())
                ->add('id', HiddenType::class, [
                    'data' => $user->getId()
                ])
                ->getForm();

            $options['form'] = $form->createView();
        }
        return $this->render($view, $options);
    }

    /**
     * Admin : delete user (after confirm)
     * @Route("/admin/user/delete", name="admin_users_delete")
     * @IsGranted("ROLE_ADMIN")
     * @param User $user
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(UserRepository $repo, FileService $fileService, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createFormBuilder(array())
            ->add('id', HiddenType::class)
            ->getForm();
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {

                $user = $repo->find($form['id']->getData());
                
                // User to delete is current user
                if ($user === $this->getUser()) {
                    throw $this->createAccessDeniedException("Vous ne pouvez pas vous supprimer vous même");
                }

                $oldAvatar = $user->getAvatar();
                
                //Delete profil image
                if (!empty($oldAvatar)) {
                    $fileService->deleteFile($this->getParameter('avatars_directory') . $oldAvatar);
                }
                
                $manager->remove($user);
                $manager->flush();

                $this->addFlash(
                    'success',
                    "L'utilisateur a bien été supprimé !"
                );
            }
        } else {
            $this->addFlash(
                'warning',
                "Vous n'avez pas l'autorisation de supprimer cet utilisateur !"
            );
        }
        
        return $this->redirectToRoute('admin_users_index');
    }
}
