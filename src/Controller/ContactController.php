<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Service\EmailService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ContactController extends AbstractController
{
    /**
     * Display contact form for popin modal and send message
     * @Route("/contact", name="contact")
     * @return Response
     */
    public function index(Request $request, ParameterBagInterface $params, EmailService $mailer)
    {

        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contactFormData = $form->getData();

            $subject = 'Message de contact - Snowtricks';
            $content = $this->renderView(
                'emails/contact.html.twig',
                [
                    'formData' => $contactFormData,
                    'dateSend' => new \DateTime()
                ],
                'text/html'
            );
            
            $result = $mailer->sendMessage(
                $subject,
                $content,
                $params->get('email-contact'),
                $params->get('email-noreply')
            );
            return $this->json($result);
        }

        return $this->render('front/blocs/modal-contact.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
