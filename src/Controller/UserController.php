<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use App\Entity\Traits\PostTrait;
use App\Service\PaginationService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController
{
    use PostTrait;

    /**
     * Display only online public posts for an user
     * @Route("/user/{slug}/{page<\d+>?1}", name="user_show")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginationService $pagination, $page, User $user)
    {
        if (!$user->getActive()) {
            throw $this->createNotFoundException('Cet utilisateur est inconnu');
        }

        if ($user == $this->getUser()) {
            return $this->redirectToRoute('account_index');
        }
        
        $pagination->setEntityClass(Post::class)
            ->setPage($page)
            ->setLimit(6)
            ->setMethod('findPublicPublishedPostOrderByUser')
            ->setSortBy('dateCreate')
            ->setOrder('DESC')
            ->setTemplatePath('front/_partials/pagination.html.twig')
            ->setArguments(['user' => $user, 'privateCat' => false ])
            ->setRoute('user_show')
            ->setPaginationSlug($user->getSlug());

        return $this->render('front/pages/user/index.html.twig', [
            'user' => $user,
            'pagination' => $pagination,
            'footerPost' => $this->getPostByCatSlug($this->getParameter('about_slug'))
        ]);
        
    }
}
