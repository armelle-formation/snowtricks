<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AdminAccountController extends AbstractController
{
    /**
     * @Route("/admin/login", name="admin_account_login")
     */
    public function login(AuthenticationUtils $utils)
    {
        
        $error = $utils->getLastAuthenticationError();
        $username = $utils->getLastUsername();

        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('admin_index');
        } else {
            return $this->render('admin/pages/account/login.html.twig', [
                'error' => $error,
                'username' => $username
            ]);
        }
        
    }

    /**
     * @Route("/admin/logout", name="admin_account_logout")
     * @return void
     */
    public function logout()
    {
        // ...
    }
}
