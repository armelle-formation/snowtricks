<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentAdminType;
use App\Service\PaginationService;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminCommentController extends AbstractController
{
    /**
     * Admin : display list of comments
     * @Route("/admin/comments/{page<\d+>?1}", name="admin_comments_index")
     * @param int $page
     * @param PaginationService $pagination
     * @return Response
     */
    public function index($page, PaginationService $pagination)
    {
        $pagination->setEntityClass(Comment::class)
            ->setPage($page)
            ->setSortBy('dateCreate')
            ->setOrder('DESC');

        return $this->render('admin/pages/comment/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * Admin : edit a comment
     * @Route("/admin/comments/{id}/edit", name="admin_comments_edit")
     * @return void
     */
    public function edit(Comment $comment, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(CommentAdminType::class, $comment);
        $form->handleRequest($request);

        //Clear Flash
        $this->get('session')->getFlashBag()->clear();

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
            $comment->setDateUpdate(new \DateTime());

            $manager->persist($comment);
            $manager->flush();

            $this->addFlash(
                'success',
                "Le commentaire n°<strong>#{$comment->getId()}</strong> a bien été mis à jour !"
            );

            return $this->redirectToRoute('admin_comments_index');

            } else {
                $this->addFlash(
                    'warning',
                    "une erreur s'est produite, le commentaire n'a pas été mis à jour !"
                );
            }
        }
        
        return $this->render('admin/pages/comment/edit.html.twig', [
            'form' => $form->createView(),
            'comment' => $comment
        ]);
    }

    

    /**
     * Admin : Display popin with confirmation message for delete a comment
     * @Route("/admin/comments/confirm/delete/{id}", name="admin_comments_confirm_delete")
     * @return Response
     */
    public function confirmDelete(int $id)
    {
        $form = $this->createFormBuilder(array())
            ->add('id', HiddenType::class, [
                'data' => $id
            ])
            ->getForm();

        return $this->render('admin/blocs/confirm-modal-form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Confirmation de suppression',
            'text' => 'Etes vous sur de vouloir supprimer ce commentaire ?',
            'action' => $this->generateUrl('admin_comments_delete'),
            'idForm' => 'confirm-delete-' . $id
        ]);

    }

    /**
     * Admin : delete comment (after confirm)
     * @Route("/admin/comments/delete", name="admin_comments_delete")
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(CommentRepository $repo, Request $request, EntityManagerInterface $manager)
    {
                
        $form = $this->createFormBuilder(array())
            ->add('id', HiddenType::class)
            ->getForm();
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {
                $comment = $repo->find($form['id']->getData());

                $manager->remove($comment);
                $manager->flush();

                $this->addFlash(
                    'success',
                    "Le commentaire a bien été supprimé !"
                );
            }
        } else {
            $this->addFlash(
                'warning',
                "Vous n'avez pas l'autorisation de supprimer ce commentaire !"
            );
        }
        
        return $this->redirectToRoute('admin_comments_index');
    }
}
