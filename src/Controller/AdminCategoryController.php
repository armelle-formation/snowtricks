<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use App\Service\PaginationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminCategoryController extends AbstractController
{
    /**
     * Admin : display list of categories
     * @Route("/admin/category/{page<\d+>?1}", name="admin_category_index")
     * @param int $page
     * @param PaginationService $pagination
     * @return Response
     */
    public function index($page, PaginationService $pagination)
    {
        $pagination->setEntityClass(Category::class)
            ->setPage($page)
            ->setSortBy('title');

        return $this->render('admin/pages/category/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * Admin : add a new category
     * @Route("/admin/category/add", name="admin_category_add")
     * @param Category $category
     * @return Response
     */
    public function add(Request $request, EntityManagerInterface $manager)
    {
        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        //Clear Flash
        $this->get('session')->getFlashBag()->clear();

        if ($form->isSubmitted()) {

            if ($form->isValid()) {

                $manager->persist($category);
                $manager->flush();

                $this->addFlash(
                    'success',
                    "La catégorie <strong>{$category->getTitle()}</strong> a bien été mise à ajoutée !"
                );

                return $this->redirectToRoute('admin_category_index');

            } else {
                $this->addFlash(
                    'warning',
                    "une erreur s'est produite, la catégorie n'a pas été enregistrée !"
                );
            }
        }

        return $this->render('admin/pages/category/edit.html.twig', [
            'form' => $form->createView(),
            'category' => $category
        ]);
    }

    /**
     * Admin : edit a category
     * @Route("/admin/category/{id}/edit", name="admin_category_edit")
     * @return void
     */
    public function edit(Category $category, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        
        //Clear Flash
        $this->get('session')->getFlashBag()->clear();

        if ($form->isSubmitted()) {
            
            if ($form->isValid()) {

                $manager->persist($category);
                $manager->flush();

                $this->addFlash(
                    'success',
                    "La catégorie <strong>{$category->getTitle()}</strong> a bien été mise à jour !"
                );

            } else {
                $this->addFlash(
                    'warning',
                    "une erreur s'est produite, la catégorie n'a pas été mise à jour !"
                );
            }
        }
        
        return $this->render('admin/pages/category/edit.html.twig', [
            'form' => $form->createView(),
            'category' => $category
        ]);
    }

    /**
     * Admin : Display popin with confirmation message for delete a category
     * @Route("/admin/categories/confirm/delete/{id}", name="admin_categories_confirm_delete")
     * @return Response
     */
    public function confirmDelete(Category $category)
    {
        $options = [];
        if (count($category->getPosts()) > 0) {
            $view = 'admin/blocs/modal-info.html.twig';
            $options['title'] = 'Confirmation de suppression';
            $options['text'] = "Vous ne pouvez pas supprimer cette catégorie car elle possède des articles !";
    
        } else {
            $view = 'admin/blocs/confirm-modal-form.html.twig';
            $options['title'] = 'Confirmation de suppression';
            $options['text'] = 'Etes vous sur de vouloir supprimer cette categorie ?';
            $options['action'] = $this->generateUrl('admin_categories_delete');
            $options['idForm'] = 'confirm-delete-' . $category->getId();

            $form = $this->createFormBuilder(array())
                ->add('id', HiddenType::class, [
                    'data' => $category->getId()
                ])
                ->getForm();

            $options['form'] = $form->createView();
        }
        return $this->render($view, $options);
    }

    /**
     * Admin : delete category (after confirm)
     * @Route("/admin/category/delete", name="admin_categories_delete")
     * @param Category $category
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(CategoryRepository $repo, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createFormBuilder(array())
            ->add('id', HiddenType::class)
            ->getForm();
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {
                $category = $repo->find($form['id']->getData());

                $manager->remove($category);
                $manager->flush();

                $this->addFlash(
                    'success',
                    "La catégorie a bien été supprimée !"
                );
            }
        } else {
            $this->addFlash(
                'warning',
                "Vous n'avez pas l'autorisation de supprimer cette catégorie !"
            );
        }
        
        return $this->redirectToRoute('admin_category_index');
    }
}
