<?php

namespace App\Service;

use Exception;
use Swift_Mailer;
use Swift_Message;
use Twig\Environment;

/**
 * Class EmailService
 */
class EmailService
{

    private $mailer;

    private $twig;

    /**
     * EmailService constructor.
     * @param Swift_Mailer $mailer
     * @param Environment $twig
     */
    public function __construct(Swift_Mailer $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    /**
     *  Send a mail with swiftmailer with a twig template
     *
     * @param string $subject
     * @param [type] $content
     * @param string $recipient
     * @param string $sender
     * @return int
     */
    public function sendMessage(string $subject, $content, string $emailRecipient, string $emailSender)
    {
        
        $sender = [$emailSender => 'Snowtricks'];
        $return = [
            "result" => 0,
            "label" => "warning",
            "message" => "Une erreur est survenue. L'email n'a pu être envoyé."
        ];

        try {

            $response = $this->mailer->send((new Swift_Message($subject))
                ->setFrom($sender)
                ->setReplyTo($sender)
                ->setTo($emailRecipient)
                ->setBody($content)
                ->setContentType("text/html")
            );

            if ($response) {
                $return = [
                    "result" => 1,
                    "label" => "success",
                    "message" => "Merci, votre message a bien été envoyé. Nous vous répondrons dans les plus brefs délais."
                ];
            }
        
        } catch (Exception $e) {}

        return $return;
    }
    
    /**
     * Send a mail with mail php function with a twig template
     * @param string $destinataire
     * @param string $content
     * @param array $aParams
     * @throws BlogException
     */
    public function sendMailTwig(string $subject, string $content, string $destinataire, array $expediteur): void
    {
        //Headers
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'From:'.$expediteur['name'].' <'.$expediteur['email'].'>' . "\r\n" .
            'Reply-To:'.$expediteur. "\r\n" .
            'Content-Type: text/html; charset="utf-8"; DelSp="Yes"; format=flowed '."\r\n" .
            'Content-Disposition: inline'. "\r\n" .
            'Content-Transfer-Encoding: 7bit'." \r\n" .
            'Return-Path: '.$expediteur."\r\n";
            'X-Mailer:PHP/'.phpversion();
        $subject = '=?UTF-8?B?'.base64_encode($subject).'?=';

        // Envoi du mail
        try {
            mail($destinataire, $subject, $content, $headers);
        } catch (Exception $e) {
            throw new Exception(
                $e->getMessage(),
                $e->getCode(),
                "Problème d'envoi d'email",
                'EmailException', __FUNCTION__
            );
        }
    }
}
