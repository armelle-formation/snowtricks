<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;

/**
 * Pagination class
 * Requires entity class after instantiation
 */
class PaginationService
{
    /**
     * Entity to be paged
     *
     * @var string
     */
    private $entityClass;

    /**
     * The number of records to retrieve
     *
     * @var integer
     */
    private $limit = 10;

    /**
     * Current page
     *
     * @var integer
     */
    private $currentPage = 1;

    /**
     * Doctrine manager to find the entityClass repository
     *
     * @var ObjectManager
     */
    private $manager;

    /**
     * Twig template for rendering
     *
     * @var Twig\Environment
     */
    private $twig;

    /**
     * Name of routes for pagination
     *
     * @var string
     */
    private $route;

     /**
     * Arguments for repo Method and/or for render
     *
     * @var string
     */
    private $paginationSlug;

    /**
     * Template path
     *
     * @var string
     */
    private $templatePath;

    /**
     * SortBy
     *
     * @var string
     */
    private $sortby = 'id';

    /**
     * Order sort
     *
     * @var string
     */
    private $order = 'ASC';

    /**
     * Method of repo
     *
     * @var string
     */
    private $method = 'findAllByOrder';

    /**
     * Arguments for repo Method and/or for render
     *
     * @var array
     */
    private $arguments;

     /**
     * Constructor
     * N'oubliez pas de configurer votre fichier services.yaml afin que Symfony sache quelle valeur
     * utiliser pour le $templatePath
     * @param ObjectManager $manager
     * @param Environment $twig
     * @param RequestStack $request
     * @param string $templatePath
     */
    public function __construct(EntityManagerInterface $manager, Environment $twig, RequestStack $request, $templatePath)
    {
        $this->twig         = $twig;
        $this->templatePath = $templatePath;
        $this->manager      = $manager;
        $this->route        = $request->getCurrentRequest()->attributes->get('_route');
    }


    public function setEntityClass(string $entityClass): self
    {
        $this->entityClass = $entityClass;
        return $this;
    }

    public function getEntityClass(): string
    {
        return $this->entityClass;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setPage(int $page): self
    {
        $this->currentPage = $page;
        return $this;
    }

    public function getPage(): int
    {
        return $this->currentPage;
    }

    public function setRoute(string $route): self
    {
        $this->route = $route;
        return $this;
    }

    public function getRoute(): string
    {
        return $this->route;
    }
    
    public function setPaginationSlug(string $paginationSlug): ?self
    {
        $this->paginationSlug = $paginationSlug;
        return $this;
    }

    public function getPaginationSlug(): ?string
    {
        return $this->paginationSlug;
    }


    public function getParamsRoute(): array
    {
        return $this->paramsRoute;
    }

    public function setTemplatePath(string $templatePath): self
    {
        $this->templatePath = $templatePath;

        return $this;
    }

    public function getTemplatePath(): string
    {
        return $this->templatePath;
    }

    public function setSortBy(string $sortby): self
    {
        $this->sortby = $sortby;

        return $this;
    }

    public function getSortBy(): string
    {
        return $this->sortby;
    }

    public function setOrder(string $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getOrder(): string
    {
        return $this->order;
    }

    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function setArguments(array $arguments): self
    {
        $this->arguments = $arguments;

        return $this;
    }

    public function getArguments(): array
    {
        return $this->arguments;
    }

     /**
     * Allows you to display the navigation rendering within a twig template
     * - page  => current page
     * - pages => the total number of pages
     * - route => the name of route to use for navigation links
     * Warning: this function does not return anything, it directly displays the rendering
     * @return void
     */
    public function display()
    {
        $this->twig->display($this->templatePath, [
            'page' => $this->currentPage,
            'pages' => $this->getPages(),
            'route' => $this->route,
            'arguments' => $this->arguments,
            'paginationSlug' => $this->getPaginationSlug()
        ]);
    }

    /**
     * Allows to retrieve the number of pages that exist on a particular entity
     * @throws Exception if the entityClass property is not passed
     * @return int
     */
    public function getPages(): int
    {
        if (empty($this->entityClass)) {
            throw new \Exception("Aucune entité spécifiée ! Utilisez la méthode setEntityClass() de votre objet PaginationService");
        }
        $total = count($this->getData(true));
        return ceil($total / $this->limit);
    }

    /**
     * Allows to retrieve paginated data for a specific entity
     * @throws Exception if the entityClass property is not passed
     * @return array
     */
    public function getData($total = false)
    {
        if (empty($this->entityClass)) {
            throw new \Exception("Aucune entité spécifiée ! Utilisez la méthode setEntityClass() de votre objet PaginationService");
        }
        
        $args = [
            'sortby' => $this->sortby,
            'order' => $this->order
        ];

        if (!$total) {
            $offset = $this->currentPage * $this->limit - $this->limit;
            $args['limit'] = $this->limit;
            $args['offset'] = $offset;
        }
        

        if (!empty($this->arguments)) {
            $args = array_merge_recursive(
                $args,
                $this->arguments
            );
        }
        return $this->manager
                ->getRepository($this->entityClass)
                ->{$this->method}($args);
    }

    public function getTotal()
    {
        return count($this->getData(true));
    }
}
