<?php

namespace App\Service;

use Exception;
use App\Entity\Post;
use Doctrine\Common\Collections\Collection;

class VideoService
{
    /**
     * Transform url passed to an embed URL for most famous video provider
     */
    public function vidProviderUrl2Player($url)
    {
        try {
            $my_array_of_vars = parse_url($url);
            $path = $my_array_of_vars['path'];
            $query = parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_params);
            
            if (strpos($path, 'embed')) {
                return $url;
                
            // youtube
            } elseif (strpos($url, 'youtube')) {
                if (array_key_exists('v', $my_array_of_params)) {
                    return 'https://www.youtube.com/embed/' . $my_array_of_params['v'];
                }
                
            // youtu.be
            } elseif (strpos($url, 'youtu.be')) {
                $vid = substr($path, 1);
                return "https://www.youtube.com/embed/". $vid;
                
            // vimeo
            } elseif (strpos($url, 'vimeo')) {
                $vid = substr($path, 1);
                return 'https://player.vimeo.com/video/'.$vid ;
            
            // dailymotion
            } elseif (strpos($url, 'dailymotion')) {
                
                $d = strpos($path, 'video/');
                if (!$d) {
                    return '';
                }
                $f = strpos($path, '/', $d);
                $vid = substr($path, $d+$f);
                return '//www.dailymotion.com/embed/video/'.$vid;
            
            // dailymotion share
            } elseif (strpos($url, 'dai.ly')) {
                $vid = substr($path, 1);
                return '//www.dailymotion.com/embed/video/'.$vid;
                
            } else {
                return '';
            }
        } catch (Exception $e) {
            return '';
        }
    }

    /**
     * Add video to post with URL verification
     *
     * @param Post $post
     * @param Collection $aVideos
     * @return void
     */
    public function addVideosToPost(Post $post, Collection $aVideos)
    {
        $emptyUrls = 0;
        $aFlash = [];

        foreach ($aVideos as $video) {
            $url = $this->vidProviderUrl2Player($video->getUrl());
            if (!empty($url)) {
                $video->setUrl($url);
                $post->addVideo($video);
            } else {
                $emptyUrls++;
                $post->removeVideo($video);
            }
        }
        
        if ($emptyUrls > 0) {
            $aFlash = [
                'label' => 'warning',
                'message' => "Attention au moins une des URLs vidéos n'était pas conforme et n'a pas été ajoutée."
            ];
        }
        return $aFlash;
    }
}
