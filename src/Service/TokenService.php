<?php

namespace App\Service;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;

class TokenService
{

    /**
     * Generate a JWT Token
     */
    public function generateTokenForUser(JWTEncoderInterface $jwtEncoder, User $user, int $nbMinutsValidity = 30)
    {
        //Generate token
        $issuedAt = time();
        $payload = array(
            'userid' => $user->getId(),
            'email' => $user->getEmail(),
            'iat' => $issuedAt,
            'exp' => $issuedAt + (60 * $nbMinutsValidity)
        );

        return $jwtEncoder->encode($payload);

    }
}
