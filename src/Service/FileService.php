<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

/**
 * Class FileService
 */
class FileService
{
    private $fileSystem;

    public function __construct()
    {
        $this->filesystem = new FileSystem();
    }

    /**
     * Delete a file
     */
    public function deleteFile(string $path)
    {
        $result = $this->filesystem->remove($path);
        if ($result === false) {
            throw new IOException(sprintf('Error deleting "%s"', $path));
        }
    }
    /**
     * Check if a file exist in the FileSystem
     *
     * @param string|iterable $files A filename, an array of files, or a \Traversable instance to check
     * @return void
     */
    public function exists($files)
    {
        return $this->filesystem->exists($files);
    }
}
