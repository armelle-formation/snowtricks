<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploaderService
{
    private $tricksDirectory;
    private $avatarsDirectory;
    private $tricksRelPathDirectory;
    private $avatarsRelPathDirectory;

    public function __construct($tricksDirectory, $avatarsDirectory, $tricksRelPathDirectory, $avatarsRelPathDirectory)
    {
        $this->tricksDirectory = $tricksDirectory;
        $this->avatarsDirectory = $avatarsDirectory;
        $this->tricksRelPathDirectory = $tricksRelPathDirectory;
        $this->avatarsRelPathDirectory = $avatarsRelPathDirectory;
    }

    /**
     * Upload image with an unique name
     * @param UploadedFile $file
     * @param string $type
     * @return string
     */
    public function upload(UploadedFile $file, string $type)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = transliterator_transliterate(
            'Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()',
            $originalFilename
        );
        $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();
        $targetDirectory = $type == 'tricks' ? $this->tricksDirectory() : $this->avatarsDirectory();

        try {
            $file->move($targetDirectory, $fileName);
        } catch (FileException $e) {}

        return $fileName;
    }

    /**
     * Copy an image with an unique name or not
     *
     * @param string $path
     * @param string $type
     * @param boolean $uniqueName
     * @return void
     */
    public function copy(string $path, string $type, $uniqueName = true)
    {
        $fileSystem = new Filesystem();
        $fileinfo = pathinfo($path);
        $safeFilename = transliterator_transliterate(
            'Any-Latin; Latin-ASCII; [^A-Za-z0-9_-] remove; Lower()',
            $fileinfo['filename']
        );
        $newFileName =
            $uniqueName ?
            $safeFilename.'-'.uniqid().'.'.$fileinfo['extension'] :
            $safeFilename.'.'.$fileinfo['extension']
        ;
        $targetDirectory = $type == 'tricks' ? $this->tricksDirectory() : $this->avatarsDirectory();
        $fileSystem->copy($path, $targetDirectory . $newFileName);

        return $newFileName;
    }

    public function tricksDirectory()
    {
        return $this->tricksDirectory;
    }

    public function avatarsDirectory()
    {
        return $this->avatarsDirectory;
    }

    public function tricksRelPathDirectory()
    {
        return $this->tricksRelPathDirectory;
    }

    public function avatarsRelPathDirectory()
    {
        return $this->avatarsRelPathDirectory;
    }
}
