<?php
 
namespace App\Service;

use HTMLPurifier;
 
class PurifierService
{
    /**
     * Sanitize HTML content send by ckeditor
     */
    public function purify($dirty_html)
    {
        $purifier = new HTMLPurifier();
        return $purifier->purify($dirty_html);
    }
}
