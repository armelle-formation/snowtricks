<?php

namespace App\Entity\Traits;

use App\Repository\PostRepository;

trait PostTrait
{

    private $entityRepository;

    /**
     * Returns the last post of the footer category
     * @required
     * @param PostRepository $repository
     */
    public function setEntityRepository(PostRepository $repository)
    {
        $this->entityRepository = $repository;
    }

    public function getPostByCatSlug(string $slug)
    {
        return $this->entityRepository->findOneByCategorySlug($slug)[0];
    }
}
