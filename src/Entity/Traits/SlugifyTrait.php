<?php

namespace App\Entity\Traits;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\PropertyAccess\PropertyAccess;

trait SlugifyTrait
{
    
    /**
    * @ORM\Column(type="string", length=255)
     */
    private $slug;

    
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(): self
    {
        $this->slug = $this->initSlug();

        return $this;
    }


    /**
     * Initialize slug
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @return void
     */
    public function initSlug()
    {
        $slugify = new Slugify();
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        //User slug
        if ($propertyAccessor->isReadable($this, 'firstname')) {
            $this->slug = $slugify->slugify($this->firstname . ' ' . $this->lastname);
        } else {
            $this->slug = $slugify->slugify($this->title);
        }
    }
}
