<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class AccountControllerTest extends WebTestCase
{

    public function testLogin()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
        $this->assertResponseStatusCodeSame(HttpFoundationResponse::HTTP_OK);
        
        if ($client->getResponse()->isSuccessful()) {
            $form = $crawler->selectButton('Connexion')->form();
            $form['_username'] = getEnv('A_USERNAME');
            $form['_password'] = getEnv('A_PASSWORD');
            $client->submit($form);
            $this->assertTrue($client->getResponse()->isRedirection());
            
            $crawler = $client->followRedirect();
            $this->assertContains('Déconnexion',$client->getResponse()->getContent());
        }
    }
    
    public function testRegistration()
    {
        $client = static::createClient();
        $client->request('GET', '/register');

        $this->assertResponseStatusCodeSame(HttpFoundationResponse::HTTP_OK);
    }

}