<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class AdminControllerTest extends WebTestCase
{

    public function testLogin()
    {
        $client = static::createClient();
        $client->request('GET', '/admin');

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}