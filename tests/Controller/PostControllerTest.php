<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class PostControllerTest extends WebTestCase
{

    public function testShow()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/tricks/mute');
        $this->assertSame(1, $crawler->filter('div.blog-details-text')->count());
    }

    public function test404Error()
    {
        $client = static::createClient();
        $client->request('GET', '/tricks/fezfzef');
        $this->assertResponseStatusCodeSame(HttpFoundationResponse::HTTP_NOT_FOUND);
    }

}