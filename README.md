# Snowtricks

This project is a training project. 
It's the 6th Project of PHP / Symfony web developer cursus of OpenClassRooms 

The final website contains a community Blog about snowboard tricks. 
All texts of FrontOffice part are customizables via the BackOffice part.
The final result is visible : [Snowtricks Online](http://snowtricks.oc-armellebraud.fr)


## Technical specifications
* Apache 2.4.38
* PHP 7.3.6
* MySQL 5.0.12
* Bootstrap theme
* Libraries PHP installed via composer

## Requirements

##### Environment #####
* Symfony 5.0.5
* Composer 1.9.0
* Bootstrap 4.3.1
* jQuery 3.4.1
* PHPUnit 6.5.14

##### Bundles #####
  * LexikJWTAuthentification
  * SwiftMailer
  * Twig
  * Phpunit
  * fzaninotto Faker (dev only)
  
##### PHP Library #####
  * Ezyang Htmlpurifier 4.12
  * cocur/slugify
  * Font-awesome 5.9.0
    
##### Extensions #####
  * pdo

## Installation 
* Clone the repository
```
  git clone https://gitlab.com/armelle-formation/snowtricks.git
```

* Install composer on your server following "Command-line installation" here => https://getcomposer.org/download/

* Install dependencies.
```
  composer install
```
## Configuration
* Duplicate the .env.dist file and customize it with your database identifiers
```
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name"`
```
* Rename this file in .env and upload it on your server

## Customization
* Customize /config/services.yaml parameters : email adresses, punchline, images directory, name of default image, locale...
* Create an uploads folder in public folder and its tree (like you have configured in service.yaml). For your information, original tree is :
```
└── uploads
    ├── avatars
    └── tricks
```
* If you don't want to use the fixtures you must copy the default trick images in your tricks images directory and the default avatar image in your avatar images directory.
You can recover those of the projects in /src/DataFixtures/img-fixtures/avatars and /src/DataFixtures/img-fixtures/tricks

## Database
Import snowtricks.sql file in your database 

Your can also generate fake content (warning : you must be in dev environnement : remember to update your .env file)
* Create database 
```
  php bin/console doctrine:database:create
```
* Get tables 
```
  php bin/console doctrine:migrations:migrate
```
* Generate content
```
  php bin/console doctrine:fixtures:load  
```

## Access
Whether you use the .sql file or fixtures, an admin account is already created : Username => admin, Password => password // If you keep using this website, create new account and delete this one

## Tests
* The database comes with a user already registered to make tests (username = 'admin', password = 'password')

* Configure the phpunit.xml file
```
<env name="DATABASE_URL" value="mysql://db_user:db_password@127.0.0.1:3306/db_name"/>
<env name="A_USERNAME" value="admin"/>
<env name="A_PASSWORD" value="password"/>
```
* run in console `php bin/phpunit --coverage-html tests/report.html` 
* see results in console and on report html page (/tests/report.html)


## Code Quality
Medal A on SonarQube (HTML, CSS ans PHP PSR-2)  
![alt text](public/assets/img/quality_gate.jpg)

## Project status
Development has slowed down but not stopped. You can contact me if you need to help or more informations

##Copyright and License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</a>.
