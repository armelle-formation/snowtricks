-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  sam. 07 mars 2020 à 20:30
-- Version du serveur :  10.2.31-MariaDB
-- Version de PHP :  7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ocarugfu_snowtricks`
--

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `private` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `title`, `slug`, `private`) VALUES
(7, 'Rotations', 'rotations', 0),
(8, 'Grabs', 'grabs', 0),
(9, 'Flips', 'flips', 0),
(10, 'Slides', 'slides', 0),
(11, 'Qui suis-je ?', 'qui-suis-je', 1),
(12, 'A propos', 'a-propos', 1),
(13, 'Straight Airs', 'straight-airs', 0);

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` int(11) NOT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `post_id`, `author_id`, `date_create`, `content`, `status_id`, `date_update`) VALUES
(10, 16, 18, '2020-02-29 09:24:37', 'Ce trick semble basique mais il demande une bonne maîtrise technique ! Il ne faut pas se lancer à la légère !', 2, '2020-03-07 18:11:14'),
(11, 17, 16, '2020-02-22 04:40:48', 'Bien costaud ! Quelq\'un a déjà réussi à faire ce trick ? Au bout de combien de temps ?', 2, '2020-03-07 20:27:20'),
(12, 21, 14, '2020-02-06 05:56:25', 'Super basique, mais super cool !', 2, '2020-03-07 20:28:00'),
(14, 23, 16, '2020-02-25 15:07:49', 'Déjà essayé... Pas facile... J\'aurais bien besoin d\'un bon professeur !', 1, '2020-03-07 20:26:50'),
(16, 26, 14, '2020-01-19 09:53:28', 'Vraiment bizarre comme trick ! Entrainement sur trampoline en amont vivement conseillé', 1, '2020-03-07 20:28:17'),
(17, 25, 12, '2020-03-07 20:29:10', 'Super les photos de ton article ! T\'en as d\'autres ?', 2, '2020-03-07 20:29:19'),
(18, 25, 12, '2020-03-07 20:29:22', 'Super les photos de ton article ! T\'en as d\'autres ?', 1, '2020-03-07 20:29:22');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `image`
--

INSERT INTO `image` (`id`, `post_id`, `name`) VALUES
(47, 16, 'mute_adobestock_1039528_preview-5e639409c6215.jpeg'),
(48, 16, 'mute_adobestock_215053346_preview-5e639409c69b0.jpeg'),
(49, 16, 'mute_adobestock_216121965_preview-5e639409c7187.jpeg'),
(50, 26, 'canadian_bacon_adobestock_81343950_preview-5e63d89f3befa.jpeg'),
(51, 29, 'japan_air_adobestock_311811783_preview-5e63ddb55b40c.jpeg'),
(53, 15, 'ollie_adobestock_257493355_preview-5e63e44b0120e.jpeg'),
(54, 15, 'ollie_adobestock_14120363_preview-5e63e49381b07.jpeg'),
(55, 23, 'shifty_adobestock_304635000_preview-5e63e56a3e01a.jpeg'),
(56, 21, 'backflip_adobestock_12854256_preview-5e63eb5d01eef.jpeg'),
(57, 21, 'backflip_adobestock_253380017_preview-5e63eb5d04381.jpeg'),
(58, 18, '5050_adobestock_7193720_preview-5e63ee9eedc3d.jpeg'),
(59, 18, '5050_adobestock_139768655_preview-5e63ee9eee48b.jpeg'),
(60, 18, '5050_adobestock_228746129_preview-5e63ee9eeee86.jpeg'),
(61, 18, '5050_adobestock_245210110_preview-5e63ee9eef51f.jpeg'),
(62, 18, '5050_adobestock_317917769_preview-5e63ee9eefc54.jpeg'),
(63, 17, 'backside_rodeo_1080_adobestock_286887053_preview-5e63f054a314d.jpeg'),
(64, 17, 'backside_rodeo_1080_adobestock_286887054_preview-5e63f054a3844.jpeg'),
(65, 25, 'indy_adobestock_319982149_preview-5e63f0e660611.jpeg'),
(66, 30, 'tailgrab_adobestock_116557709_preview-5e63f2d9ec98e.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20191223092422', '2020-03-02 18:03:15'),
('20191223094719', '2020-03-02 18:03:15'),
('20200108130650', '2020-03-02 18:03:15'),
('20200113142026', '2020-03-02 18:03:15'),
('20200120092811', '2020-03-02 18:03:15'),
('20200120094124', '2020-03-02 18:03:15'),
('20200120203907', '2020-03-02 18:03:15'),
('20200122124215', '2020-03-02 18:03:15'),
('20200125133348', '2020-03-02 18:03:16'),
('20200125180422', '2020-03-02 18:03:16'),
('20200125184904', '2020-03-02 18:03:16'),
('20200126161915', '2020-03-02 18:03:16'),
('20200129165725', '2020-03-02 18:03:16'),
('20200129173845', '2020-03-02 18:03:16'),
('20200129181706', '2020-03-02 18:03:16'),
('20200202140844', '2020-03-02 18:03:16'),
('20200202171900', '2020-03-02 18:03:16'),
('20200203171232', '2020-03-02 18:03:16'),
('20200204130059', '2020-03-02 18:03:16'),
('20200209133213', '2020-03-02 18:03:16'),
('20200216223520', '2020-03-02 18:03:16'),
('20200220114909', '2020-03-02 18:03:16'),
('20200229171058', '2020-03-02 18:03:16'),
('20200304131011', '2020-03-04 17:22:59');

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heading` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime DEFAULT NULL,
  `cover_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `date_publish` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `post`
--

INSERT INTO `post` (`id`, `title`, `heading`, `content`, `active`, `date_create`, `date_update`, `cover_image`, `slug`, `author_id`, `category_id`, `date_publish`) VALUES
(15, 'Ollie', 'Inspiré du skate, c\'est le trick freestyle de base qui permet à la planche de décoller du sol. Tout comme en skate, il existe quatre façons de la faire, selon le sens et si l\'impulsion vient du nose ou du tail : ollie, switch ollie, nollie, fakie ollie.', '<p>Les Ollies sont l\'une des fugures les plus essentielles à apprendre en matière de snowboard. Que vous fassiez des sauts de parc, des coups latéraux, des jibs urbains ou du freeride, l\'ollie est le moyen le plus efficace de prendre l\'air.</p>\n\n<p>Les Ollies sont essentiels car ils utilisent l\'énergie du flex de votre planche pour vous faire monter. Essayez de sauter sur un terrain plat ... vous n\'êtes pas devenu très haut, n\'est-ce pas? Maintenant, laissez-nous vous apprendre à Ollie et obtenez de la hauteur dans ce saut!</p>\n\n<h3>Plus de flexibilité, plus de hauteur !</h3>\n\n<p>Poussez votre planche vers l\'avant, glissez-la sous vous et équilibrez vos hanches au-dessus de la queue de votre planche. Dans cette position, votre planche se pliera et se soulèvera du sol dans une position de presse de queue. Utilisez cette presse pour sortir, de votre queue et dans les airs, en atterrissant uniformément sur les deux pieds.</p>\n\n<p>Commencez à ajouter du poids à votre pied avant, pour vous aider à placer votre planche sous vous. Cela vous aide à obtenir plus de flexibilité et plus de hauteur hors de votre Ollie. Bien qu\'il soit bon d\'avoir une idée du mouvement Ollie sur le plat, la plupart des snowboarders trouvent plus facile d\'effectuer un Ollie en mouvement et de sauter pendant un ride.</p>\n\n<h3>Une question de timing</h3>\n\n<p>Défiez le timing de vos Ollies en utilisant quelque chose comme une boule de neige, un gant ou même un bâton comme marqueur de position au sol pour sauter et chronométrer votre pop. Quand il s\'agit de faire des sauts en snowpark de parc, le timing compte beaucoup, donc cet exercice vous aidera vraiment à composer votre timing!</p>\n\n<p><em>Source : <a href=\"https://snowboardaddiction.com/blogs/jumping/how-to-ollie-1\">How to Ollie</a> - Snowboard Addiction</em></p>', 1, '2020-02-23 00:55:02', '2020-03-07 19:14:43', 'ollie_adobestock_198179464_preview-5e63e493810d0.jpeg', 'ollie', 17, 13, '2020-02-29 00:00:00'),
(16, 'Mute', 'Avec sa ma main avant, le rider saisit le bord de la planche entre ses pieds ou devant son pied avant.', '<p><span style=\"font-size:11pt;\"><span style=\"font-family:\'Century Gothic\', sans-serif;\">Un Mute Grab n’est pas vraiment un trick mais cela vaut vraiment la peine d\'essayer car elle corrigera les problèmes de posture mineurs qui découlent d\'une conduite légèrement ouverte, améliorant ainsi votre positionnement global en style libre.</span></span><span style=\"font-size:11pt;\"><span style=\"font-family:\'Century Gothic\', sans-serif;\">Un Mute Grab est l\'endroit où la main avant saisit le bord de l\'orteil entre les pieds. La planche est maintenue à plat.</span></span></p>\n\n<h5>Approche</h5>\n\n<p><span style=\"font-size:11pt;\"><span style=\"font-family:\'Century Gothic\', sans-serif;\">Commencez directement derrière le kicker à un point qui vous permettra d\'atterrir en toute sécurité sur le dessus de la table ou juste au-dessus de l\'articulation. Recréez une forme d\'entonnoir avec vos virages en vous concentrant sur la conduite droite au centre du kicker.</span></span></p>\n\n<h6>Décollage</h6>\n\n<p><span style=\"font-size:11pt;\"><span style=\"font-family:\'Century Gothic\', sans-serif;\">Visez à être une base plate lorsque vous remontez le kicker avec le haut du corps aligné avec la planche. Vous avez la possibilité de maintenir cette base plate ou de transférer légèrement la pression sur vos orteils lorsque vous lancez un pop ou un ollie. Vous pouvez monter le kicker légèrement plus bas que la normale pour réduire la distance de déplacement pour la benne.</span></span></p>\n\n<h3>Tour</h3>\n\n<p><span style=\"font-size:11pt;\"><span style=\"font-family:\'Century Gothic\', sans-serif;\">La saisie muette peut initialement sembler gênante, mais persévérer. Saisissez le bord des orteils entre les fixations avec votre main avant.</span></span></p>\n\n<p><em>Source : <a href=\"https://medium.com/@maverixsnow/how-to-mute-grab-on-a-snowboard-b11553f7f37f\">How To mute grab on a snowboard</a> - Medium</em></p>', 1, '2019-12-27 21:11:02', '2020-03-07 13:36:27', 'mute_adobestock_188091891_preview-5e639409c56ce.jpeg', 'mute', 16, 8, '2020-02-07 23:00:00'),
(17, 'Backside Rodeo 540 Melon', 'Rotation désaxée de 3 tours effectuée tête en bas. Rotation mélangée à un backflip avec une implusion sur les talons.', '<h3>1. La préparation</h3>\n\n<p>Cela pourrait être une astuce plus difficile, mais vous devriez prendre la course et la transition exactement de la même manière que les 180 et 360, en mettant en place votre poids centré, votre planche à plat et un peu de pression sur le bord de vos orteils.</p>\n\n<h3>2. Le pop</h3>\n\n<p>Maintenant pour la partie amusante! Lorsque vous atteignez la lèvre, vous voulez faire éclater comme vous le feriez pour un 540 arrière afin que vous soyez sûr d\'obtenir suffisamment de rotation pour vous déplacer. Pendant l\'éclatement, cependant, vous devrez jeter votre tête sur votre épaule arrière afin de lancer l\'élément hors axe du tour.</p>\n\n<h3>3. Le grab</h3>\n\n<p>Vous avez vraiment besoin de vous présenter le tableau si vous voulez saisir cette astuce. Si vous deviez même essayer de tendre la main, vous êtes dans un monde en difficulté et vous atterrirez très probablement sur votre cou! Pas beaucoup de plaisir et un bon tracas pour la patrouille de ski vous gratter de la pente. Dans cette photo, je prends du melon - je trouve que c\'est la prise la plus facile pour aider à faire tourner la rotation - mais encore une fois, si vous trouvez d\'autres prises plus faciles avec des backflips droits et 540, essayez-les.</p>\n\n<h3>4. Continuez à saisir</h3>\n\n<p>Continuez à tenir la benne aussi longtemps que vous le pouvez, mais sur cette astuce, je dirais que c\'est encore plus important de le faire, car l\'ouverture arrêtera la rotation et vous risquez bien de la tourner, ce qui ne sera pas joli.</p>\n\n<h3>5. L’atterissage</h3>\n\n<p>Parce que vous venez sur un axe étrange, vous pouvez voir votre atterrissage très loin, ce qui aide. La seule partie difficile est que vous atterrissez aveugle comme vous le feriez sur un arrière 180 ou 540, donc les mêmes règles d\'atterrissage s\'appliquent: regardez entre vos fixations au sol, repérez votre atterrissage et plantez les deux pieds en même temps –Avec un peu de pression sur le bord des orteils pour arrêter la rotation. Il est trop facile de revenir immédiatement si vous ne faites pas attention et que la pression sur le bord des orteils est la clé pour l\'arrêter. Après vous être assuré que vous avez fait tout ce que vous devriez être en mesure de désactiver l\'interrupteur, juste à temps pour le dernier appel!</p>\n\n<p>Source : <a href=\"https://whitelines.com/snowboarding-advice/trick-tips/kicker/how-to-backside-rodeo-540-melon.html\">How to Backside Rodeo 540 melon</a> - Whitelines.com</p>', 1, '2019-09-16 21:17:10', '2020-03-07 20:04:52', 'backside_rodeo_1080_adobestock_205818718_preview-5e63f054a261f.jpeg', 'backside-rodeo-540-melon', 18, 7, '2020-02-15 04:00:00'),
(18, 'Frontside 50-50', 'Un trick qui consiste à glisser sur un rail en gardant la planche parallèle à l\'obstacle sur laquelle elle glisse.', '<p>Le 50-50 est probablement le plus simple de tous les trucs de jibbing et le premier que vous apprendrez. Fondamentalement, cela signifie rouler directement sur une boîte ou un rail. En soi, ce n\'est pas le mouvement le plus excitant du monde, mais lorsque vous le liez dans un combo, les choses deviennent bientôt plus intéressantes. Ces trois variantes sont idéales pour le type de boîte plate que vous trouverez dans la plupart des parcs et des dômes de neige, alors pourquoi ne pas donner une chance aux 50-50?</p>\n\n<h3>1. L\'approche</h3>\n\n<p>Tout d\'abord - vous devez trouver un bon rail à plat ou une boîte avec laquelle vous êtes à l\'aise, faire quelques courses en faisant des 50-50 simples et simplement vous habituer à la vitesse dont vous aurez besoin pour la frapper. Lorsque vous êtes prêt à essayer quelque chose de nouveau, approchez-vous du rail avec une base plate (attention à ne pas aller trop vite, sinon vous finirez par décoller de l\'extrémité de la partie plate et manquerez la section inférieure, ce qui vous fera atterrir sur ton cul!). Votre poids doit être centré sur la planche - restez détendu avec les genoux légèrement pliés et gardez les yeux sur le prix.</p>\n\n<h3>2. Le pop</h3>\n\n<p>Certains rails / boîtes sur lesquels vous pouvez simplement monter, mais la plupart du temps, vous devrez un peu éclater. Sautez de votre pied arrière en utilisant du flex dans la queue de votre planche pour vous aider à sauter. Le kicker fera cependant beaucoup de travail, alors ne sautez pas trop fort ou vous pourriez finir par manquer la boîte. Essayez d\'atterrir parfaitement en ligne avec la boîte et centré sur votre planche avec les genoux pliés. Faites attention de ne pas attraper un bord - gardez votre base à plat.</p>\n\n<h3>3. Positionnement</h3>\n\n<p>Gardez votre poids centré tout au long du 50/50, mais lorsque vous approchez de la partie inférieure de la boîte, déplacez légèrement votre poids vers le pied avant et placez votre pied arrière devant vous. Encore une fois, assurez-vous de garder la planche à plat pour ne pas glisser sur votre bord.</p>\n\n<h3>4. Style</h3>\n\n<p>Une fois que vous avez atteint la position de glissement de la planche, ajustez votre pied arrière sur le côté tout en gardant vos épaules alignées avec la boîte. Vos yeux doivent rester concentrés sur la fin de la section vers le bas. Non seulement l\'âge supplémentaire sera-t-il malade, il vous aidera également lorsque vous arriverez au bout du rail et que vous devrez ramener votre planche à terre.</p>\n\n<h3>5. L\'atterrissage</h3>\n\n<p>Bon, voici maintenant la partie délicate qui sépare les hommes des garçons. Restez concentré et confiant, prêt à vous lancer. Lorsque vous approchez de l\'extrémité du rail, donnez-lui un peu de pop et sucez un peu vos genoux pour absorber l\'impact. Vous devriez atterrir avec la planche à plat mais légèrement sur le bord de vos orteils afin de garder le contrôle. Préparez-vous maintenant à partir et à le réclamer.</p>\n\n<p>Source : <a href=\"https://snowboardaddiction.com/blogs/jibbing/how-to-frontside-50-50\">How to frontside 50-50</a> - Snowboard Addiction</p>', 1, '2019-10-02 21:59:28', '2020-03-07 19:57:41', '5050_adobestock_106996650_preview-5e63ee9eed02c.jpeg', 'frontside-50-50', 15, 10, '2020-02-14 00:00:00'),
(19, 'Cork', 'Un Cork, appellation tirée du mot anglais corkscrew signifiant « tire-bouchon », est une rotation horizontale dont l\'axe peut varier. Les meilleurs snowboardeurs réussissent des « Triple Cork ».', '<p>Un cork est une rotation verticale mais aussi horizontale plus ou moins désaxée, selon un mouvement d\'épaules effectué juste au moment du saut.</p>\n\n<p>Fondamentalement, c\'est un flip hors axe lancé en arrière avec un spin (le plus souvent 540º ou \"Rodeo 5\"). Tire-bouchon ou «Cork»: Le skieur effectue une rotation hors axe ou inversée horizontale distincte. À aucun moment, les pieds du skieur ne doivent être au-dessus de leur tête. Double Cork ou «Dub Cork»: Le skieur effectue deux rotations hors axe distinctes.</p>\n\n<p><em>Source : <a href=\"http://snomie.com/snowboard-trick-definitions-names-cork-cab/\">Snowboard trick definitions - Snomie.com</a></em></p>', 1, '2019-11-01 15:51:40', '2020-03-07 20:25:08', 'cork_adobestock_124488050_preview-5e63ed13a7e42.jpeg', 'cork', 17, 9, '2020-03-08 21:00:00'),
(20, 'Frontside 180', 'Rotation d’un tour complet à plat (planche à l’horizontale) effectuée dans le sens des aiguilles d’une montre et orientée vers le carre backside', '<p>Le Frontside 180 est une des rotations la plus facile à apprendre sur un snowboard. Pour bien le réaliser il y a une astuce devenue intemporelle qui, lorsqu\'elle est exécutée avec le bon style, résiste bien aux triplés le plus noueux.</p>\n\n<h3>Par où commencer ?</h3>\n\n<p>En roulant sur une pente douce, commencez par effectuer des 180 sur le sol. Engagez votre talon avec la neige, amorcez votre virage sur le talon et continuez à tourner jusqu\'à ce que vous rouliez dans le sens opposé sur la pente. Vous roulez maintenant dans une position de changement.</p>\n\n<p>Pouvoir rouler en switch est crucial pour les 180 et les tonnes d\'autres figures de freestyle, alors soyez à l\'aise avec !</p>\n\n<p>De là, revenez simplement à votre position de conduite habituelle en utilisant votre talon. Vous venez de terminer vos premiers Frontside 180 et Switch Frontside 180</p>\n\n<h3>Pourquoi est-il appelé \"Frontside\"?</h3>\n\n<p>C\'est ce qu\'on appelle une rotation avant car la face avant de votre corps est orientée vers le bas tout au long des 180 degrés de rotation.</p>\n\n<h3>Ajouter une rotation</h3>\n\n<p>Pendant que vous faites glisser le snow sur la neige, ajoutez des mouvements de rotation avec le haut de votre corps. Balancer vos bras et vos épaules dans le virage est la clé pour créer l\'élan requis pour le 180.</p>\n\n<p>Dans le même temps, essayez également de retirer votre poids de la planche en étendant légèrement vos genoux, mais gardez la planche en contact avec la neige. Cette technique vous permet d\'être à l\'aise avec la sensation de sauter un Front 1, sans réellement sauter un Front 1.</p>\n\n<h3>Frontside 180\'s On Flats</h3>\n\n<p>Si vous essayez de sauter un Frontside 180 à partir d\'une base plate, vous sentirez votre planche glisser. Utilisez plutôt vos carres pour saisir la neige. Créez un léger bord d\'orteil, remontez pour créer l\'élan et sautez de votre bord d\'orteil pour faire tourner le Frontside 180.</p>\n\n<p><span style=\"font-size:11pt;\"><span style=\"font-family:\'Century Gothic\', sans-serif;\">Facile hein ? Il est beaucoup plus difficile d\'utiliser le bord du talon sur une surface plane, mais essayez-le également car la plupart des rotations Frontside sont effectuées à partir du bord du talon.</span></span></p>\n\n<h3>Frontside 180\'s en traversant</h3>\n\n<p>Parcourez une pente sur le bord de votre orteil pour faire votre premier Frontside 180.</p>\n\n<p>Créez une adhérence avec la neige en utilisant le bord de vos orteils</p>\n\n<p>Remontez le haut de votre corps à l\'aide des bras et des épaules</p>\n\n<p>Lorsque vous sautez du bord de vos orteils, balancez votre bras arrière sur votre corps pour qu\'il pointe dans la nouvelle direction de déplacement</p>\n\n<p>Essayez maintenant de chronométrer ces mouvements avec une bosse dans la neige pour obtenir une hauteur supplémentaire. Tout en traversant sur le bord de votre talon, essayez quelques Frontside 180 en utilisant un Ollie puissant. Vous devez Ollie haut car c\'est un moyen plus difficile de faire tourner le Frontside 180.</p>\n\n<h3>L\'importance du pop</h3>\n\n<p>Au fur et à mesure que vous vous améliorez avec vos Front 1, il est important d\'ajouter de la pop en étendant vos jambes lorsque vous quittez le décollage. Plus vous obtenez de hauteur, vous aurez besoin de moins de rotation tout au long de votre spin. Vos 180 se déplaceront plus facilement et les rendront super flottants.</p>\n\n<p><em>Source : <a href=\"\">How to Shifty Frontside 180</a> - Snowboard Addiction</em></p>', 1, '2019-09-29 22:32:41', '2020-03-07 20:01:59', 'frontside_adobestock_83915360_preview_90-5e63efa76ac1e.jpeg', 'frontside-180', 13, 7, '2020-02-29 01:00:00'),
(21, 'Backflip', 'Le backflip ou l\'art de se retourner en arrière lors d\'un saut, figure parmi les sauts les plus spectaculaires de cette discipline.', '<p>Le fameux Backflip, un trick très fun et facile à maitriser que l’on peut placer à beaucoup d’endroits. On peut aussi l’appeler rodéo back 3.6 si on le tourne un peu sur le côté, mais c’est le même mouvement.</p>\n\n<h3>Petits conseils en vrac</h3>\n\n<ul><li>Le mieux c’est de s’entrainer à le faire sur un trampoline car le mouvement est le même.</li>\n	<li>Choisissez un kicker de bord de piste, qui kicke un peu de préférence, pour vous aider à envoyer facilement</li>\n	<li>Arrivez bien fléchi en appui sur les 2 jambes et fixez le bout du kicker. L’impulsion se fait à 2 pieds au bout du kicker et pas avant : si on envoie trop tôt on risque de taper la tête dans le kicker ou de trop tourner, de faire un tour et demi et de tomber sur la tête. Deux situations à éviter...</li>\n	<li>Donc impulsion à deux pieds, et on envoie la tête en arrière pour chercher le mouvement. Dès que l’on a décollé il faut remonter les genoux pour enrouler le mouvement. Les profs de gym ont tendance à dire que l’on envoie le mouvement avec le bassin, ce qui n’est pas faux mais c’est surtout quand on a compris le mouvement et que l’on est à l’aise avec.</li>\n	<li>Donc regrouper les jambes en les montant. A ce moment on peut aussi penser à grabber mais ce n’est pas obligé pour commencer... On continue d’emmener la rotation avec la tête en arrière.</li>\n	<li>Très vite on peut voir la réception et on va pouvoir gérer la fin de al rotation soit en se tendant un peu pour la ralentir, soit en se regroupant encore davantage pour tourner plus vite.</li>\n	<li>Replacez bien la board sous votre corps avant d’atterrir, et amortir en pliant les jambes</li>\n</ul><p><em>Source : <a href=\"https://www.snowsurf.com/snow-tricks-comment-faire-backflip-snowboard\">Comment faire un un backflip en snowboard</a> - Snowsurf</em></p>\n\n<p> </p>', 1, '2019-12-03 02:04:53', '2020-03-07 19:48:27', 'backflip_adobestock_10151781_preview-5e63ebb0ca34e.jpeg', 'backflip', 17, 9, '2020-02-23 23:00:00'),
(23, 'Shifty', 'Une figurer aérienne dans laquelle un snowboardeur tord son corps afin de déplacer ou faire pivoter sa planche d\'environ 90 ° par rapport à sa position normale sous lui, puis ramène la planche à sa position d\'origine avant d\'atterrir. Cette figure peut être exécutée en recto ou en verso, et également en variation avec d\'autres astuces et tours.', '<p>Le Frontside Shifty n\'est pas seulement un exercice de snowboard visant à améliorer vos compétences en freestyle, c\'est un truc de saut élégant qui, combiné à certaines prises, fait ressortir votre conduite.</p>\n\n<h3>Et comment apprend-on cela ?</h3>\n\n<p>La façon la plus simple d\'apprendre Shifties est sur un trampoline.</p>\n\n<ol><li>Rebondissez en position neutre</li>\n	<li>Saut de 90 degrés sur le devant</li>\n	<li>Puis au prochain rebond, retour à une position de conduite imaginaire</li>\n</ol><p>Faites cela un certain nombre de fois afin de commencer à sentir votre contre-corps supérieur et inférieur tourner l\'un contre l\'autre, se tordre puis se détendre.</p>\n\n<p>Il est maintenant temps de tout faire en un seul saut. Tout comme votre premier baiser, ne vous précipitez pas ! Assurez-vous de prendre l\'air avant de faire le mouvement Shifty, c\'est la torsion dans l\'air qui rend cette astuce possible.</p>\n\n<h3>Sur la colline</h3>\n\n<p>Il est maintenant temps de pratiquer tout en descendant la pente.</p>\n\n<ol><li>Descendez en ligne droite</li>\n	<li>Frontside Shifty pendant quelques secondes (en gardant ce bord d\'orteil vers le haut!)</li>\n	<li>Revenez ensuite à une position de conduite normale</li>\n</ol><p>N\'oubliez pas d\'utiliser le haut de votre corps pour contrer la rotation du mouvement, à la fois dans et hors de la glissière.</p>\n\n<h3>Sautons !</h3>\n\n<p>Pour votre premier Frontside Shifty, entrez dans le saut avec une base plate comme vous le feriez pour un air droit. Gardez les genoux pliés pour gardez une position solide.</p>\n\n<p>Dans les airs, utilisez le mouvement de contre-rotation pratiqué plus tôt pour effectuer le Frontside Shifty. Ne vous précipitez pas et ne vous inquiétez pas si vos premières Shifties ne sont que petites, elles deviendront plus grandes à mesure que vous deviendrez plus efficace dans les mouvements.</p>\n\n<h3>The Perfect Shifty</h3>\n\n<p>Le Shifty parfait est lorsque votre planche est tournée de 90 degrés au milieu du saut. La meilleure façon d\'y parvenir est de mettre la main sur la queue de votre planche.</p>\n\n<p>N\'oubliez pas, ne vous inquiétez pas si vos premières quarts se sentent difficiles et en désordre. Ils deviendront beaucoup plus faciles avec la pratique sur et hors de la colline. À partir de toute cette pratique, vous lancerez des Frontside Shifties élégants sur toutes sortes de sauts et de coups latéraux, avec toutes sortes de prises différentes.</p>\n\n<p><em>Source : <a href=\"https://snowboardaddiction.com/blogs/jumping/how-to-frontside-shifty-on-a-snowboard\">How ton frontside shifty on a snowboard</a> - Snowboard Addiction</em></p>', 1, '2020-02-02 10:19:59', '2020-03-07 19:18:25', 'shifty_adobestock_122252770_preview-5e63e56a3d69a.jpeg', 'shifty', 17, 13, '2020-02-08 14:00:00'),
(25, 'Indy', 'Considéré comme la base du grab, l\'Indy consiste à saisir sa planche entre les pieds à l\'aide de sa main arrière.', '<p><span style=\"font-size:11pt;\"><span style=\"font-family:\'Century Gothic\', sans-serif;\">Un indy grab, généralement appelé simplement indy, est un trick de skateboard. C\'est un trick aérien de la catégorie des grabs durant lequel le snowboarder saisit le milieu de sa planche, entre ses deux pieds, sur le côté où pointent ses orteils.</span></span></p>\n\n<p><span style=\"font-size:11pt;\"><span style=\"font-family:\'Century Gothic\', sans-serif;\">L\'indy est effectué depuis les années \'70. Il est généralement utilisé en rampe (half-pipes...), bien qu\'il soit également possible de le faire en flat (sur un sol plat). Ce grab est un des tricks de base de la pratique en vert\' et il est généralement combiné à des rotations, des kicklips et des heelflips.</span></span></p>\n\n<p><span style=\"font-size:11pt;\"><span style=\"font-family:\'Century Gothic\', sans-serif;\">À l\'origine, le trick était appelé Indy Air. Inventé par Duane Peters et popularisé par Tony Alva, il consistait à faire un backside air (180° au-dessus d\'une rampe, en faisant face à celle-ci) tout en attrapant sa planche du côté des orteils avec la main arrière. Ayant fait du chemin de skateur à skateur, la figure a donné naissance à de nombreuses variantes, poussant les limites de la créativité des skateboarders. Une des variantes les plus célèbres est le kickflip indy.</span></span></p>\n\n<p><span style=\"font-size:11pt;\"><span style=\"font-family:\'Century Gothic\', sans-serif;\">Le nom indy grab est apparu à cause d\'un manque de connaissance de la distinction entre un indy air et un frontside air. L\'utilisation de cette appellation pour nommer le grab en lui-même vient d\'une erreur de nomenclature. Parce qu\'il est erronément appelé ainsi dans le jeu vidéo Tony Hawk\'s Pro Skater, beaucoup de gens appellent dorénavant indy le simple fait d\'attraper sa planche de cette manière, sans tenir compte du sens de la rotation, et même s\'il n\'y a pas de rotations du tout.</span></span></p>\n\n<p><em>Source : <a href=\"https://fr.wikipedia.org/wiki/Indy_grab\">Indy Grab</a> -  Wikipedia</em></p>', 1, '2020-03-07 20:22:24', '2020-03-07 20:19:37', 'indy_adobestock_311811783_preview-5e63f0e65fb51.jpeg', 'indy', 18, 8, '2020-02-27 09:00:00'),
(26, 'Canadian Bacon', 'La main arrière passe derrière la jambe arrière pour saisir le bord de la planche entre les fixations pendant que la jambe arrière est désaxée.', '<h3>Le grab</h3>\n\n<p>Cette prise vous oblige à passer par derrière pour saisir la planche entre les jambes. Ne vous focalisez pas trop sur cette dernière phrase. Le bacon canadien n\'est pas aussi compliqué qu\'il y paraît - même s\'il semble quand même bizarre au début ! </p>\n\n<h3>La torsion</h3>\n\n<p>Ce trick intrigue et en général les spectateurs y regardent à deux fois. Il faut un peu de temps pour comprendre comment atteindre la prise. Bien que ce trick ne soit jamais devenu courant, lorsqu\'il est ramené avec une manœuvre frontale comme Tim Eddy, le Canadian Bacon peut aider à se sentir vraiment cool.</p>\n\n<h3>Un peu d\'histoire</h3>\n\n<p>Inventée en 1988/89, la légende du snowboard Jason Ford se souvient que \"<em>Brushie a commencé à faire celle-ci en premier</em>\". Brushie n’en est pas trop sûr. \"<em>Je ne pense pas avoir commencé le bacon canadien</em>\", dit-il. «<em>[Mais] j\'aurais pu tout simplement me défoncer. La plupart de ces saisies ont eu lieu en été sur les trampolines. Je ne me souciais pas vraiment des saisies entre les jambes en général. Ce n\'était vraiment qu\'une phase rapide pour moi tout en essayant de penser à de nouvelles astuces. Étant donné que vos pieds ne se détachaient pas de la planche et que nous ne tournions pas comme des fous, différentes prises étaient la seule chose à proposer. Je n\'ai pas fait ces prises depuis très longtemps. J\'ai vraiment essayé de rester fidèle au Snwoboard, et pour moi, c\'était la mauvaise direction</em>. »</p>\n\n<p><em>Source : <a href=\"https://www.snowboarder.com/videos/how-to-grab-canada-bacon-on-a-snowboard/\">How to grab a Canadian Bacon on a snowboard</a> - Snowboarder.com</em></p>', 1, '2019-06-13 12:58:58', '2020-03-07 18:26:35', 'canadian_bacon_adobestock_5755868_preview-5e63d89f3b522.jpeg', 'canadian-bacon', 15, 8, '2020-03-07 14:00:00'),
(27, 'Qui suis je ?', NULL, '<p>Je m’appelle Lou, j’ai 31 ans et je vis à Toulouse, dans le Sud-Ouest de la France. Pas très loin de la montagne, pas trop loin de l’océan. Je dévale les pentes enneigées des Pyrénées l’hiver et j’aime surfer au Pays Basque ou dans les Landes l\'été. Je passe le reste du temps sur mon skate (sauf les jours de pluie).</p>\n\n<p>N\'hésitez à m’envoyer un petit mot...</p>', 1, '2019-06-13 12:58:58', '2020-03-04 18:31:09', 'about-5e5fe5dc5efd4.jpeg', 'qui-suis-je', 12, 11, '2010-07-23 01:00:00'),
(28, 'A propos', NULL, 'Ce site est un projet d\'étude réalisé avec le framework Symfony 5 dans le cadre du parcours de formation d\'OpenClassRooms : Développeur d\'application PHP/Symfony.', 1, '2019-06-13 12:58:58', '2020-03-04 18:25:35', NULL, 'a-propos', 12, 12, '1997-10-03 15:06:12'),
(29, 'Japan Air', 'Un Japan Air est essentiellement un Mute Grab ajusté derrière le dos.', '<p><span style=\"font-size:11pt;\"><span style=\"font-family:\'Century Gothic\', sans-serif;\">Un Japan Air est super divertissant à regarder et encore plus amusant à piétiner. Ajouter du Japon à vos tours est un moyen sûr d\'obtenir des accessoires de vos amis.</span></span></p>\n\n<h3>Trois billets pour Tokyo</h3>\n\n<ol><li>Prenez un Mute et lancez-vous en avant en jetant votre autre bras en arrière</li>\n	<li>Gardez votre bras à l\'extérieur de votre genou, en dehors du genou mais à l\'intérieur des fixations.</li>\n	<li>Pliez les genoux et cambrez le dos.</li>\n</ol><p><span style=\"font-size:11pt;\"><span style=\"font-family:\'Century Gothic\', sans-serif;\">Ce trick necessiterait de prendre quelques cours de yoga</span></span><span style=\"font-size:13px;\"> </span></p>\n\n<p><span style=\"font-size:11pt;\"><span style=\"font-family:\'Century Gothic\', sans-serif;\">Ce trick est un peu plus avancé, vous devriez donc avoir de l\'expérience avec la plupart des grabs de base avant de prendre votre envol.</span></span></p>\n\n<p><em><span style=\"font-size:11pt;\"><span style=\"font-family:\'Century Gothic\', sans-serif;\">Source : <a href=\"https://snowboardaddiction.com/blogs/tramp-board-training/how-to-japan\">How To Japan</a> de Snowboard Addiction</span></span></em></p>', 1, '2020-03-07 18:45:25', '2020-03-07 18:45:54', 'japan_air_adobestock_319421540_preview-5e63ddb55a8cc.jpeg', 'japan-air', 12, 8, '2020-03-07 18:00:00'),
(30, 'Tail Grab', 'Le tindy est un grab de snowboard dans lequel le snowboarder saisit avec sa main arrière entre la fixation arrière et la queue de la planche sur le bord des orteils. Son nom est dérivé d\'une combinaison du Grab indy et du Tail Grab.', '<p>Le Grab tindy est généralement considéré comme une forme médiocre en snowboard.</p>\n\n<h3>Commencez par le Nose Grab</h3>\n\n<p>Lors d\'un saut, vous êtes à l\'aise avec le mouvement des jambes pour un Nose Grab, mais n\'allez pas encore le saisir. C\'est une sensation gênante de ne pas plier les deux jambes uniformément en l\'air, alors assurez-vous de laisser le Cambre exactement de la même manière qu\'un saut régulier pour rester en équilibre dans l\'air. Lorsque vous vous sentez à l\'aise, optez pour le Nose Grab.</p>\n\n<h3>Bon style contre. Mauvais style</h3>\n\n<p>Faites attention à l\'endroit où se trouve votre main pour vous assurer de composer dans un bon style. Pour le Nose &amp; Tail Grab, gardez-la toujours sur le bout et la spatule arrière de votre planche..</p>\n\n<p>Vous pouvez commencer à ajouter du style supplémentaire en mettant davantage l\'accent sur vos extensions, en fléchissant davantage vos jambes lorsque vous attrapez et en incluant de petites quantités de Shifty. Un Frontside Shifty fonctionne bien avec un Nose Grab et un petit Backside Shifty fonctionne bien avec un Tail Grab.</p>\n\n<p>Source : <a href=\"https://snowboardaddiction.com/blogs/jumping/how-to-nose-tail-grab\">Jumping to Nose Tail Grab</a> – Snowboard Addiction</p>', 1, '2020-03-07 20:15:37', '2020-03-07 20:17:37', 'tailgrab_adobestock_37458945_preview-5e63f2d9e69b0.jpeg', 'tail-grab', 19, 8, '2020-03-07 20:00:00'),
(31, 'Rocket Air', 'La main avant saisit le bord de l\'orteil devant le pied avant (mute) et la jambe arrière est désaxée tandis que la planche pointe perpendiculairement au sol.', '<p>Étant donné que le décollage est le même que lorsque vous faites de l\'air droit, ce n\'est pas difficile. Cependant, avant le décollage, si vous passez à un mouvement de saisie, vous perdrez l\'équilibre.</p>\n\n<p>Par conséquent, assurez-vous d\'abord de décoller la tranche de la même manière que pour Straight Air</p>\n\n<ol><li>Dès que vous décollez, tendez votre jambe avant avec une certaine intensité</li>\n	<li>Tout en faisant attention à ne pas avoir la poitrine complètement tournée vers l\'avant, tendez les deux mains.</li>\n	<li>Avant de saisir votre planche, attention, si votre poitrine est trop tournée vers l\'avant, vous partirez en rotation.</li>\n	<li>Avec les deux mains, exercez une certaine force et en tirant la planche vers vous, étendez complètement votre jambe arrière</li>\n	<li>Tenez la planche jusqu’à l\'atterrissage.</li>\n	<li>Même si votre poitrine est tournée vers l\'avant, faites attention à ce que vos hanches ne tournent pas vers le côté.</li>\n	<li>Vous pouvez empêcher la rotation en l\'air en ne permettant pas à vos hanches de tourner!</li>\n	<li>Même si vous relâchez le Grab une fois que la queue de la planche a touché le sol, à ce stade, l\'atterrissage reste sûr.</li>\n	<li>L\'atterrissage est exactement le même que pour Straight Air</li>\n	<li>La clé est de se plier complètement et d\'étirer les jambes.</li>\n	<li>En pliant et en étirant les jambes à moitié, il est plus difficile de faire le Grab lui-même.</li>\n</ol>', 1, '2020-03-07 20:23:29', '2020-03-07 20:23:29', 'rocket_air_adobestock_69355771_preview-5e63f4b1acf73.jpeg', 'rocket-air', 12, 8, '2020-03-07 20:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id`, `title`, `name`) VALUES
(2, 'ROLE_ADMIN', 'Administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(2, 12),
(2, 20);

-- --------------------------------------------------------

--
-- Structure de la table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `status`
--

INSERT INTO `status` (`id`, `name`) VALUES
(1, 'A modérer'),
(2, 'Validé'),
(3, 'Masqué');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pseudo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `pseudo`, `email`, `avatar`, `hash`, `active`, `date_create`, `date_update`, `slug`) VALUES
(12, 'Jeanne', 'Doe', 'dodo', 'admin', NULL, '$2y$13$ZwxgJErgbXQNIVkgE6bTCe1x4TT7s4EWeOQ6GHnXkusP3makERWO.', 1, '1999-10-24 19:06:42', '2020-03-04 18:25:27', 'jeanne-doe'),
(13, 'Daniel', 'Guillot', 'pariatur', 'guy.gay@wanadoo.fr', 'adobestock_202830484_g-5e5fe4886ccc6.jpg', '$2y$13$wrcM7pqUbK.Wi7USKII36eojsn4A.TkvCUHxJD6BOtJDQpkHPciD6', 1, '2019-10-09 17:08:30', '2020-03-04 18:25:28', 'daniel-guillot'),
(14, 'René', 'Petitjean', 'ab', 'roland.gallet@orange.fr', 'adobestock_202830484_b-5e5fe48919ead.jpg', '$2y$13$TAxOHGekWqKHATXFBKAe6ughxR5efL20egFJTV7hdDsHv7lkPfyAC', 1, '2019-04-11 05:48:40', '2020-03-04 18:25:29', 'rene-petitjean'),
(15, 'Marcel', 'Perret', 'aliquid', 'madeleine18@sauvage.com', 'adobestock_215365067_1000-5e5fe6dcebfaf.jpeg', '$2y$13$Nd/O7cva1yl8IeC4WcXRa.t4KNOESH72XLCcYtAix80kC9sY2/9BG', 1, '2019-07-03 21:35:03', '2020-03-04 18:35:24', 'marcel-perret'),
(16, 'Thérèse', 'Valette', 'provident', 'eric.letellier@sfr.fr', 'adobestock_202830484_a-5e5fe48a6e605.jpg', '$2y$13$96ZB29izZi7zEysjfGDUQO4BiVf4bqUU7LgEDxoP898A9t6TX72eO', 0, '2020-02-24 17:52:47', '2020-03-07 20:25:37', 'therese-valette'),
(17, 'Astrid', 'Reynaud', 'odit', 'kmalet@henry.fr', 'adobestock_202830484_k-5e5fe48c6acd3.jpg', '$2y$13$6GHJQD.fr/ZQF2LUbigYaOh.isBTSK559F6cyksb1HXSNnpFi5JvC', 1, '2020-02-12 15:13:19', '2020-03-07 19:13:59', 'astrid-reynaud'),
(18, 'Sylvie', 'Loiseau', 'numquam', 'elisabeth.delorme@wagner.com', 'adobestock_202830484_w-5e5fe48d123d4.jpg', '$2y$13$1/acC6AFQX5lk7jjQWWEUuVnjiCiMtEbw4MEgu1qR/2yuqJsNDdR6', 1, '2019-09-23 20:57:37', '2020-03-07 14:00:49', 'sylvie-loiseau'),
(19, 'Gérardin', 'Mentvuca', 'Gege', 'braud.a@free.fr', 'adobestock_202830484_c-5e6359249188a.jpeg', '$2y$13$mBysycRATsNd47ZFWIqI8.doOR51RkSqyrKYWAo83PC.9ROSXugFC', 1, '2020-03-07 09:19:47', '2020-03-07 15:44:09', 'gerardin-mentvuca'),

-- --------------------------------------------------------

--
-- Structure de la table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `video`
--

INSERT INTO `video` (`id`, `post_id`, `url`) VALUES
(26, 15, 'https://www.youtube.com/embed/AnI7qGQs0Ic'),
(27, 15, 'https://www.youtube.com/embed/BDpxekjUCqw'),
(35, 19, 'https://www.youtube.com/embed/FMHiSF0rHF8'),
(36, 19, 'https://www.youtube.com/embed/69R-EJe-P4g'),
(40, 21, 'https://www.youtube.com/embed/SlhGVnFPTDE'),
(53, 26, 'https://www.youtube.com/embed/IVUSdEBRZ0Q'),
(54, 16, 'https://www.youtube.com/embed/CflYbNXZU3Q'),
(55, 16, 'https://www.youtube.com/embed/hih9jIzOoRg'),
(56, 26, 'https://www.dailymotion.com/embed/video/x6eaiho'),
(57, 29, 'https://www.youtube.com/embed/I7N45iRPrhw'),
(58, 23, 'https://www.youtube.com/embed/g_Pqd6VqBOg'),
(59, 23, 'https://www.youtube.com/embed/RVoP-UFewdQ'),
(60, 18, 'https://www.youtube.com/embed/e-7NgSu9SXg'),
(61, 20, 'https://www.youtube.com/embed/GnYAlEt-s00'),
(62, 30, 'https://www.youtube.com/embed/id8VKl9RVQw'),
(63, 30, 'https://www.youtube.com/embed/_Qq-YoXwNQY'),
(65, 31, 'https://www.youtube.com/embed/ySVGdt_hom4'),
(66, 31, 'https://www.youtube.com/embed/ECuPRQPmHpA');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9474526C4B89032C` (`post_id`),
  ADD KEY `IDX_9474526CF675F31B` (`author_id`),
  ADD KEY `IDX_9474526C6BF700BD` (`status_id`);

--
-- Index pour la table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C53D045F4B89032C` (`post_id`);

--
-- Index pour la table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5A8A6C8DF675F31B` (`author_id`),
  ADD KEY `IDX_5A8A6C8D12469DE2` (`category_id`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `IDX_332CA4DDD60322AC` (`role_id`),
  ADD KEY `IDX_332CA4DDA76ED395` (`user_id`);

--
-- Index pour la table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7CC7DA2C4B89032C` (`post_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT pour la table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT pour la table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526C4B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `FK_9474526C6BF700BD` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  ADD CONSTRAINT `FK_9474526CF675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `FK_C53D045F4B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`);

--
-- Contraintes pour la table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `FK_5A8A6C8DD5E258C5` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_5A8A6C8DF675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `FK_332CA4DDA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_332CA4DDD60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `FK_7CC7DA2C4B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
