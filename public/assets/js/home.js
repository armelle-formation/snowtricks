$(function() {

    $('header').removeClass('sticky');
    
    $("div.grid-item").hide()
    $("div.grid-item").slice(0, 6).show();

    $(window).on('scroll', function () {
        if ( $(window).scrollTop() > 10 ) {
            $('.navbar').addClass('white');
        } else {
            $('.navbar').removeClass('white');
        }
    });
});

//Show more tricks on home page */
function loadMore(){
   
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $("div.grid-item:hidden").slice(0, 6).slideDown();
        if ($("div.grid-item:hidden").length == 0) {
            $("#loadMore").hide();
            $("#loadLess").show();
        }
    });

    $("#loadLess").on('click', function (e) {
        e.preventDefault();
        $("div.grid-item").slice(6, $("div.grid-item").length).slideUp();
        $("#loadLess").hide();
        $("#loadMore").show();
    });
}