// PostInitForm
function initPostForm(coverImage){
    
    // Collection Videos
    addVideo($("#post_videos"));
    updateCounter('post_videos');
    handleDeleteButtons();

    // CoverImage
    initCoverImageUpload($("#post_coverImage"), coverImage, $("#post_coverChanged"));

    // CkEditor
    CKEDITOR.replace('post_content');

    // Switch
    $('[data-toggle="switch"]').bootstrapSwitch();

}
//Upload coverImage
function initCoverImageUpload(input, aImageCover, coverCounter){

    let defaultPreviewContent = "<img src='/uploads/tricks/default-trick.jpg' alt='Image par défaut'><h6 class='text-muted'>Cliquez pour changer l'image</h6>";
    const options = soloImgUploadInit(defaultPreviewContent,aImageCover,'trick');

    input.fileinput(options);

    //Trigger change image
    input.on('change', function(e) {
        e.preventDefault();
        e.stopPropagation();
        coverCounter.val(1);
    });

    //Clear fileinput after a remove
    $(".kv-file-remove").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        coverCounter.val(1);
        input.fileinput('clear');
    });

}

//Upload images gallery
function initFileUpload(input, fieldConcat, aUrls, aImagesInfos){

    let jsonArr = [];

    for (let i = 0; i < aImagesInfos.length; i++) {
        jsonArr.push({
            downloadUrl: false,
            size: aImagesInfos[i].size,
            showRemove: true,
            key:i,
            fileId: aImagesInfos[i].id
        });
    }
    const options = galleryImgUploadInit("",jsonArr,aUrls,10);
    input.fileinput(options);
    
    //Remove olds files uploaded
    if(jQuery.type(fieldConcat) === "object"){
    
        $(".kv-file-remove").click(function(e){
            e.preventDefault();
            e.stopPropagation();
            
            const valToRemove = $(this).closest(".file-preview-initial").data("fileid");
            const values = fieldConcat.val();
            let arrayValues = Array.from(values.split(','),Number);

            for (let i = 0; i < arrayValues.length; i++) {
                if (arrayValues[i] == valToRemove) { 
                    arrayValues.splice(i, 1);
                    break;
                }
            }
            
            fieldConcat.val(arrayValues.toString().split(','));

            $(this).closest(".file-preview-initial").fadeOut();
        
        });
    }
}

function addVideo(conteneur) {
    
     $('#add-video').click(function(){
        const index = +$('#widgets-counter').val();
        const tmpl = conteneur.data('prototype').replace(/__name__/g, index);
        conteneur.append(tmpl);
        $('#widgets-counter').val(index + 1);
        handleDeleteButtons();
     });
}

function handleDeleteButtons(){
    $('button[data-action="delete"]').click(function(){
        const target = this.dataset.target;
        $(target).remove();
    })
}

function updateCounter(idInput){
    const count = +$("#" + idInput + " div.form-group").length;
    $("#widgets-counter").val(count);
}