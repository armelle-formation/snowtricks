// Globals Vars
const urlUploadTricks = '/public/uploads/tricks/';
const urlUploadAvatars = '/public/uploads/avatars/';

$(function() {

    //Smooth scroll
    $('.scrollto').on('click', function() {
        const page = $(this).attr('href'); 
        const speed = 750;
        const headerHeight = $('nav').outerHeight();
        $('html, body').animate( { scrollTop: $(page).offset().top - headerHeight }, speed ); // Go
        return false;
    });
    
    $('.navbar-collapse a').click(function (e) {
        if( $(e.target).is('a') && $(e.target).attr('class') != 'nav-link dropdown-toggle' ) {
           $('.navbar-collapse').removeClass('show');
        }
    });

    $("#showMedias").on('click', function (e) {
        e.preventDefault();
        $(".carousels").removeClass('d-none');
        $("#showMedias").addClass('d-none');
        $("#hideMedias").removeClass('d-none');
    });

    $("#hideMedias").on('click', function (e) {
        e.preventDefault();
        $(".carousels").addClass('d-none');
        $("#showMedias").removeClass('d-none');
        $("#hideMedias").addClass('d-none');
    });
});

// InitfileUpload
var fileinputOptions = {
    browseClass: "btn btn-theme btn-block",
    browseLabel: "Parcourir",
    browseIcon: "<i class=\"far fa-image\"></i>",
    allowedFileTypes: ['image'],
    initialPreviewAsData: true,
    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
    initialPreviewShowDelete: true,
    initialPreviewShowUpload: false,
    language: 'fr',
    theme: 'fas',
    showBrowse: false,
    showCaption: false,
    showPreview: true,
    showRemove: false,
    showUpload: false,
    showCancel: false,
    showClose: false,
    maxImageWidth: 3000,
    maxImageHeight: 3000,
    minImageWidth: 200,
    minImageHeight: 200,
    showUploadedThumbs: true,
    enableResumableUpload: true,
    autoReplace: true,
    maxFileSize: 4 * 1024,
    overwriteInitial: false,
    fileActionSettings: {
        showZoom: true,
        showUpload: false,
        showDrag: false,
        showRemove: true
    },
    maxFileCount: 1
};


function soloImgUploadInit(defaultPreviewContent,aPreviewConfigData,env = 'tricks') {
    let url = env === 'tricks' ? urlUploadTricks : urlUploadAvatars;
    const fileInputConfig = {
        initialPreview : [ aPreviewConfigData['url'] ],
        initialPreviewConfig : [
            {size : aPreviewConfigData['size'], downloadUrl: false, showRemove: true}
        ],
        defaultPreviewContent : defaultPreviewContent,
        uploadUrl : url,
        overwriteInitial: true
    }
    $.extend( fileinputOptions, fileInputConfig );
    return fileinputOptions;
}

function galleryImgUploadInit(defaultPreviewContent,aPreviewConfigData,aData,maxFile) {
    const fileInputConfig = {
        initialPreview : aData,
        initialPreviewConfig : aPreviewConfigData,
        maxFileCount : maxFile,
        defaultPreviewContent : defaultPreviewContent,
        uploadUrl : urlUploadTricks,
        autoReplace: false
    }
    $.extend( fileinputOptions, fileInputConfig );
    return fileinputOptions;
}

// Init Avatar images
function initAvatarImageUpload(input, aAvatarImage){

    let defaultPreviewContent = "<img src='/uploads/avatars/default-avatar.png' alt='Image par défaut'><h6 class='text-muted'>Cliquez pour changer l'image</h6>";
    const options = soloImgUploadInit(defaultPreviewContent,aAvatarImage);

    input.fileinput(options);

    //Trigger change image
    input.on('change', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $("#account_avtChanged").val(1);
    });

    //Clear fileinput after a remove
    $(".kv-file-remove").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        $("#account_avtChanged").val(1);
        input.fileinput('clear');
    });
}

//Contact modal
$("#sendContactMessage").click(function(e){
    e.preventDefault();
    e.stopPropagation();

   $.ajax({
        url: '/contact',
        data: $("#contactMessageForm").serialize(),
        beforeSend: function(){
            $("#loadingoverlay").fadeIn();
        },
        method: "POST",
        dataType: "json",
        success: function(response){
            $("#loadingoverlay").fadeOut();
            $(".result-message-contact .alert").addClass("alert-" + response.label);
            $(".result-message-contact .alert").text(response.message);
        },
        complete: function() {
            setTimeout(function () {
                $('.modal').modal('hide');
            }, 3000);
        }

    });
     
});

// Back to top
$(window).scroll(function() {
    if ($(this).scrollTop() >= 600) {       // If page is scrolled more than 600px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});