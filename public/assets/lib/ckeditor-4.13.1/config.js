/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
    config.height = 450;
    config.bodyClass = 'content-post';
    config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'clipboard', groups: [ 'undo', 'clipboard' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'insert', groups: [ 'insert' ] },
        { name: 'styles', groups: [ 'styles' ] },
		'/',
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];
	config.removeButtons = 'Print,Templates,Find,Replace,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Superscript,Subscript,Strike,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,FontSize,Font,Styles,BGColor,TextColor,About';
    config.format_tags = 'p;h1;h2;h3;h4;h5;h6';
    config.removePlugins = 'resize';
    config.contentsCss = [ '/assets/lib/ckeditor-4.13.1/contents.css', '/assets/lib/ckeditor-4.13.1/ck-admin-styles.css' ],
    config.stylesSet = 'default';
    config.allowedContent = true;
};

CKEDITOR.stylesSet.add( 'default', [
    // Block Styles
    { name: 'Auteur', element: 'p', attributes: { class:"blockquote-footer" } },

    // Blockquote
    { name: 'Citation', element: 'blockquote', attributes: { 'class': 'blockquote-text'} },

    // Inline Styles
    { name: 'Gras',  element: 'span', attributes: { class:"text-bold" } },
    { name: 'Petit',  element: 'span', attributes: { class:"text-small" } },

]);
